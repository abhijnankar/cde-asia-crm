$(function() {
	// data table
	$('#itemmaster-table').DataTable({
		pageLength: 10,
		bFilter: false,
		order: [[ 1, "desc" ]],
		"columnDefs": [{
			"targets": [0,8],
			"orderable": false
		}]
	});
	
	$('#itemmaster-table-project').DataTable({
		pageLength: 10,
		bFilter: false,
		order: [[ 1, "desc" ]],
		"columnDefs": [{
			"targets": [0,7],
			"orderable": false
		}]
	});
	
	// 
	var rows = $('#itemmaster-table tr');
	
	$("input[data-select='all']").change(function(){
        $(this).prop('checked') 
            ? rows.find('.mail-check').prop('checked',true)
            : rows.find('.mail-check').prop('checked',false)        
    });
    
    // 
	var rowstable = $('#itemmaster-table-project tr');
	
	$("input[data-select='alls']").change(function(){
        $(this).prop('checked') 
            ? rowstable.find('.mail-check-project').prop('checked',true)
            : rowstable.find('.mail-check-project').prop('checked',false)        
    });
});