$(function() {

	// data table
	$('#itemmaster-table').DataTable({
		pageLength: 10,
		order: [[ 1, "desc" ]],
		"oLanguage": {
		  "sSearch": "Search By Project Code:"
		},
		"columnDefs": [{
			"targets": [6],
			"orderable": false
		}]
	});
});