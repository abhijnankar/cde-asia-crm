package com.cde.crm.repository.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.cde.crm.entity.EditListEntity;
import com.cde.crm.entity.EditPriceEntity;
import com.cde.crm.entity.EmployeeList;
import com.cde.crm.entity.IndentList;
import com.cde.crm.entity.ItemWorkEntity;
import com.cde.crm.entity.LeadsList;
import com.cde.crm.entity.VendorPOListEntity;
import com.cde.crm.entity.VendorPricingItemList;
import com.cde.crm.repository.VendorPricingListInterface;

public class VendorPricingListClass implements VendorPricingListInterface {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public List<VendorPricingItemList> vendorPricingItemList(String vendorCode) {
		String sql = "select * from cdeasia_item_master itm,  cdeasia_vendor_item_price vip where itm.ItemCode=vip.ItemCode and vip.VendorCode='"
				+ vendorCode + "' group by vip.ItemCode";
		System.out.println(sql);
		return jdbcTemplate
				.query("select * from cdeasia_item_master itm,  cdeasia_vendor_item_price vip where itm.ItemCode=vip.ItemCode and vip.VendorCode='"
						+ vendorCode + "' group by vip.ItemCode", new RowMapper<VendorPricingItemList>() {

							public VendorPricingItemList mapRow(ResultSet rs, int row) throws SQLException {
								VendorPricingItemList obj = new VendorPricingItemList();
								obj.setItemCode(rs.getString("ItemCode"));
								obj.setItemDescription(rs.getString("ItemDesc"));
								obj.setItemType(rs.getString("ItemType"));
								obj.setUnitPrice(rs.getString("UnitPrice"));
								obj.setUom(rs.getString("UOM"));
								obj.setWeight(rs.getString("Weight"));
								obj.setVendorprice(rs.getString("vendorprice"));
								obj.setLeadtime(rs.getString("lead_time"));
								obj.setUploadfile(rs.getString("upload_file"));
								obj.setFilepath(rs.getString("file_path"));
								obj.setStatus(rs.getString("status"));

								return obj;
							}

						});

	}

	public int updateVendorPrice(EditPriceEntity editPriceEntity) {
		String sql = "update  cdeasia_vendor_item_price set status='Updated', vendorprice='" + editPriceEntity.getVendorPrice()
				+ "',lead_time = '"+editPriceEntity.getLeadtime()+"',upload_file = '"+editPriceEntity.getUploadfile()+"',file_path ='"+editPriceEntity.getFilepath()+"'  "
				+ "where ItemCode='" + editPriceEntity.getEdititemcode() + "' and VendorCode='"+editPriceEntity.getvCode() + "'";
		return jdbcTemplate.update(sql);
	}

	public boolean isvendorcodeExists(String vendorCode) {
		String sql = "select count(*) from  cdeasia_vendor_item_price where VendorCode = ?";
		// System.out.println(sql);
		boolean result = false;
		int count = jdbcTemplate.queryForObject(sql, new Object[] { vendorCode }, Integer.class);
		if (count > 0) {
			result = true;
		}
		return result;
	}

	public List<EditListEntity> itemlist(String itemCode,String vcode) {
		return jdbcTemplate.query("select itm.*,cvp.vendorprice,cvp.lead_time,cvp.upload_file,cvp.file_path from cdeasia_item_master itm, cdeasia_vendor_item_price cvp where"
				+ " itm.ItemCode='" + itemCode + "' AND cvp.ItemCode = itm.ItemCode and VendorCode = '"+vcode+"'",
				new RowMapper<EditListEntity>() {

					public EditListEntity mapRow(ResultSet rs, int row) throws SQLException {
						EditListEntity ee = new EditListEntity();
						ee.setItemCode(rs.getString("ItemCode"));
						ee.setItemDescription(rs.getString("ItemDesc"));
						ee.setItemType(rs.getString("ItemType"));
						ee.setUnitPrice(rs.getString("UnitPrice"));
						ee.setVendorprice(rs.getString("vendorprice"));
						ee.setLeadtime(rs.getString("lead_time"));
						ee.setUploadfile(rs.getString("upload_file"));
						ee.setFilepath(rs.getString("file_path"));
						
						return ee;
					}

				});

	}

	public List<VendorPOListEntity> vendorPOList(String vendorCode) {
		return jdbcTemplate.query("select * from  cdeasia_purchase.cdeasia_vendorpomap vpo where vpo.VendorCode='" + vendorCode + "' group by PONumber",
				new RowMapper<VendorPOListEntity>() {

					public VendorPOListEntity mapRow(ResultSet rs, int row) throws SQLException {
						VendorPOListEntity obj = new VendorPOListEntity();
						obj.setIndentID(rs.getString("IndentId"));
						
						obj.setProjectID(rs.getString("ProjectId"));
						obj.setPoNumber(rs.getString("PONumber"));
						return obj;
					}

				});

	}

	public List<VendorPOListEntity> vendorPoItemDetails(VendorPOListEntity vendorPOListEntity) {
		return jdbcTemplate.query("select * from cdeasia_item_master itm, cdeasia_purchase.cdeasia_vendorpomap vpo "
				+ "where itm.ItemCode=vpo.ItemCode AND vpo.VendorAcceptanceStatus IS NULL and vpo.PONumber='" + vendorPOListEntity.getPoNumber() + "'",
				new RowMapper<VendorPOListEntity>() {

					public VendorPOListEntity mapRow(ResultSet rs, int row) throws SQLException {
						VendorPOListEntity obj = new VendorPOListEntity();
						obj.setItemCode(rs.getString("ItemCode"));
						obj.setItemDescription(rs.getString("ItemDesc"));
						obj.setItemType(rs.getString("ItemType"));
						obj.setUnitPrice(rs.getString("UnitPrice"));
						obj.setUom(rs.getString("UOM"));
						obj.setWeight(rs.getString("Weight"));
						obj.setPoqty(rs.getString("ArvlQty"));
						return obj;
					}

				});
		
	}

	public int updateVendorStatus(VendorPOListEntity vendorPOListEntity) {
	
		String sql="update cdeasia_purchase.cdeasia_vendorpomap set VendorAcceptanceStatus='"+vendorPOListEntity.getStatus()+"',"
				+ "delivery_charge='"+vendorPOListEntity.getDeliverycharge()+"',packing_charge='"+vendorPOListEntity.getPackingcharge()+"',"
				+ "miscellaneous_charge='"+vendorPOListEntity.getMiscellaneouscharge()+"',upload_file='"+vendorPOListEntity.getUploadfile()+"',"
				+ "comments='"+vendorPOListEntity.getComments()+"' where PONumber='"+vendorPOListEntity.getPonumber()+"' "
				+ "and ItemCode = '"+vendorPOListEntity.getItemCode()+"'";
		return jdbcTemplate.update(sql);
	}

	public List<VendorPOListEntity> vendorPORcv(String pass,String strt_date,String end_date) {
		String sql = null;
		if(!strt_date.equals("") && !end_date.equals("")) {
				sql = "select COUNT(DISTINCT PONumber) as porcv from  cdeasia_purchase.cdeasia_vendorpomap vpo "
						+ "where vpo.VendorCode = '"+pass+"' and DATE(created_at) between '"+strt_date+"' AND '"+end_date+"'";
		}
		else {			
			 sql = "select COUNT(DISTINCT PONumber) as porcv from  cdeasia_purchase.cdeasia_vendorpomap vpo "
						+ "where vpo.VendorCode = '"+pass+"'";
		}		
		System.out.println(sql);
		return jdbcTemplate.query(sql,
				new RowMapper<VendorPOListEntity>() {
					public VendorPOListEntity mapRow(ResultSet rs, int row) throws SQLException {
						VendorPOListEntity obj = new VendorPOListEntity();
						obj.setPorcv(rs.getString("porcv"));
						return obj;
					}

				});
	}

	public List<VendorPOListEntity> vendorPOAccept(String pass,String strt_date,String end_date) {
		String sql = null;
		if(!strt_date.equals("") && !end_date.equals("")) {
				sql = "select COUNT(DISTINCT PONumber) as poaccept from  cdeasia_purchase.cdeasia_vendorpomap vpo "
					+ "where VendorCode = '"+pass+"' and VendorAcceptanceStatus = 'Accept' and DATE(created_at) between '"+strt_date+"' AND '"+end_date+"'";
		}
		else {			
			 sql = "select COUNT(DISTINCT PONumber) as poaccept from  cdeasia_purchase.cdeasia_vendorpomap vpo "
					+ "where VendorCode = '"+pass+"' and VendorAcceptanceStatus = 'Accept'";
		}	
		System.out.println(sql);
		return jdbcTemplate.query(sql,
				new RowMapper<VendorPOListEntity>() {
					public VendorPOListEntity mapRow(ResultSet rs, int row) throws SQLException {
						VendorPOListEntity obj = new VendorPOListEntity();
						obj.setPoaccept(rs.getString("poaccept"));
						return obj;
					}

				});
	}

	public List<VendorPOListEntity> vendorPOReject(String pass,String strt_date,String end_date) {
		String sql = null;
		if(!strt_date.equals("") && !end_date.equals("")) {
				sql = "select COUNT(DISTINCT PONumber) as poreject from  cdeasia_purchase.cdeasia_vendorpomap vpo "
					+ "where VendorCode = '"+pass+"' and VendorAcceptanceStatus = 'Reject' and DATE(created_at) between '"+strt_date+"' AND '"+end_date+"'";
		}
		else {			
			 sql = "select COUNT(DISTINCT PONumber) as poreject from  cdeasia_purchase.cdeasia_vendorpomap vpo "
						+ "where VendorCode = '"+pass+"' and VendorAcceptanceStatus = 'Reject'";
		}	
		System.out.println(sql);
		return jdbcTemplate.query(sql,
				new RowMapper<VendorPOListEntity>() {
					public VendorPOListEntity mapRow(ResultSet rs, int row) throws SQLException {
						VendorPOListEntity obj = new VendorPOListEntity();
						obj.setPoreject(rs.getString("poreject"));
						return obj;
					}

				});
	}

	public List<VendorPOListEntity> vendorPOListLatest(String pass,String strt_date,String end_date) {
		String sql = null;
		if(!strt_date.equals("") && !end_date.equals("")) {
				sql ="select * from  cdeasia_purchase.cdeasia_vendorpomap vpo where vpo.VendorCode='" + pass + "'"
						+ " AND DATE(created_at) between '"+strt_date+"' AND '"+end_date+"'"
					+ " group by PONumber order by vpo.created_at DESC LIMIT 5";
		}
		else {			
			 sql = "select * from  cdeasia_purchase.cdeasia_vendorpomap vpo where vpo.VendorCode='" + pass + "'"
					+ " group by PONumber order by vpo.created_at DESC LIMIT 5";
		}	
		System.out.println(sql);
		
		
		return jdbcTemplate.query(sql,
				new RowMapper<VendorPOListEntity>() {

					public VendorPOListEntity mapRow(ResultSet rs, int row) throws SQLException {
						VendorPOListEntity obj = new VendorPOListEntity();
						obj.setIndentID(rs.getString("IndentId"));
						
						obj.setProjectID(rs.getString("ProjectId"));
						obj.setPoNumber(rs.getString("PONumber"));
						return obj;
					}

				});
	}

	public List<VendorPricingItemList> vendorPricingItemListLatest(String pass,String strt_date,String end_date) {
		
		String sql = null;
		if(!strt_date.equals("") && !end_date.equals("")) {
				sql ="select * from cdeasia_item_master itm,  cdeasia_vendor_item_price vip where itm.ItemCode=vip.ItemCode and vip.VendorCode='"
						+ pass + "' AND DATE(vip.created_at) between '"+strt_date+"' AND '"+end_date+"'"
						+ "group by vip.ItemCode  order by vip.created_at DESC LIMIT 5";
				//DATE(vip.created_at) between '"+strt_date+"' AND '"+end_date+"'
		}
		else {			
			 sql = "select * from cdeasia_item_master itm,  cdeasia_vendor_item_price vip where itm.ItemCode=vip.ItemCode and vip.VendorCode='"
						+ pass + "' group by vip.ItemCode  order by vip.created_at DESC LIMIT 5";
		}	
		System.out.println(sql);
		
		return jdbcTemplate
				.query(sql, new RowMapper<VendorPricingItemList>() {

							public VendorPricingItemList mapRow(ResultSet rs, int row) throws SQLException {
								VendorPricingItemList obj = new VendorPricingItemList();
								obj.setItemCode(rs.getString("ItemCode"));
								obj.setItemDescription(rs.getString("ItemDesc"));
								obj.setItemType(rs.getString("ItemType"));
								obj.setUnitPrice(rs.getString("UnitPrice"));
								obj.setUom(rs.getString("UOM"));
								obj.setWeight(rs.getString("Weight"));
								obj.setVendorprice(rs.getString("vendorprice"));

								return obj;
							}

						});
	}

	

	

	public void createindent(String indentid, String projectid, String kitid,String kittitle,String kitdesc, String bomtype, String itemcode,
			String itemdesc, String indqty, String raisedby, String raisedon) {
		String sql = "insert into cdeasia_indent set  IndentId='"+indentid+"' ,ProjectId='"+projectid+"' , KitId='"+kitid+"'"
				+ ",kittitle='"+kittitle+"',kitdescription='"+kitdesc+"',BOMType='"+bomtype+"' , ItemCode='"+itemcode+"' ,ItemDesc='"+itemdesc+"' ,"
						+ " IndQty='"+indqty+"' ,RaisedBy='"+raisedby+"',RaisedOn='"+raisedon+"' ";
			jdbcTemplate.update(sql);
		
	}

	public List<IndentList> checkitem(String items, String indentid, String projectid) {
		String sql = "select it.* from cdeasia_indent where IndentId = '"+indentid+"' and ProjectId = '"+projectid+"' AND ItemCode= '"+items+"'";
		System.out.println(sql);
		return jdbcTemplate.query("select * from cdeasia_indent",
				new RowMapper<IndentList>() {

					public IndentList mapRow(ResultSet rs, int row) throws SQLException {
						IndentList ie = new IndentList();
						ie.setIndentid(rs.getString("IndentId"));
						ie.setProjectid(rs.getString("ProjectId"));
						ie.setKitid(rs.getString("KitId"));
						ie.setBomtype(rs.getString("BOMType"));
						ie.setItemcode(rs.getString("ItemCode"));
						ie.setItemdesc(rs.getString("ItemDesc"));
						ie.setIndqty(rs.getString("IndQty"));
						ie.setRaisedby(rs.getString("RaisedBy"));
						ie.setRaisedon(rs.getString("RaisedOn"));
						ie.setStatus(rs.getString("Status"));
						ie.setCreatedat(rs.getString("created_at"));
						
						return ie;
					}

				});
	}

	public void updateindent(String indentid, String projectid, String kit, String title, String desc, String bomtype,
			String items, String itdesc, String qty, String raisedby, String raisedon) {
		String sql = "UPDATE cdeasia_indent set  IndentId='"+indentid+"' ,ProjectId='"+projectid+"' , KitId='"+kit+"'"
				+ ",kittitle='"+title+"',kitdescription='"+desc+"',BOMType='"+bomtype+"' , ItemCode='"+items+"' ,ItemDesc='"+itdesc+"' ,"
						+ " IndQty='"+qty+"' ,RaisedBy='"+raisedby+"',RaisedOn='"+raisedon+"' where ItemCode = '"+items+"' AND IndentId='"+indentid+"'"
								+ "AND ProjectId='"+projectid+"'";
			jdbcTemplate.update(sql);
		
	}

	public List<VendorPOListEntity> vendorDCList(String vcode) {
		String sql ="select * from   cdeasia_deliverychalan cd where cd.transter_to='" + vcode + "' group by cd.deliverychallan_no";
		System.out.println(sql);
		return jdbcTemplate.query("select * from   cdeasia_deliverychalan cd where cd.transter_to='" + vcode + "' group by cd.deliverychallan_no",
				new RowMapper<VendorPOListEntity>() {

					public VendorPOListEntity mapRow(ResultSet rs, int row) throws SQLException {
						VendorPOListEntity obj = new VendorPOListEntity();
						obj.setIndentID(rs.getString("indentID"));
						obj.setDeliverychallan(rs.getString("deliverychallan_no"));
						obj.setProjectID(rs.getString("ProjectId"));
						obj.setPoNumber(rs.getString("ponumber"));
						return obj;
					}

				});
	}

	public List<EditListEntity> dcitemlist(VendorPOListEntity vpe, String vcode) {
		String sql = "select itm.*,cd.dc_qty,cd.return_qty,cd.rem_qty from cdeasia_item_master itm, cdeasia_deliverychalan cd where"
				+ " cd.ItemCode = itm.ItemCode and cd.transter_to = '"+vcode+"' and cd.deliverychallan_no = '"+vpe.getDeliverychallan()+"'";
		System.out.println(sql);
		return jdbcTemplate.query("select itm.*,cd.dc_qty,cd.return_qty,cd.rem_qty from cdeasia_item_master itm, cdeasia_deliverychalan cd where"
				+ " cd.ItemCode = itm.ItemCode and cd.transter_to = '"+vcode+"' and cd.deliverychallan_no = '"+vpe.getDeliverychallan()+"'",
				new RowMapper<EditListEntity>() {

					public EditListEntity mapRow(ResultSet rs, int row) throws SQLException {
						EditListEntity ee = new EditListEntity();
						ee.setItemCode(rs.getString("ItemCode"));
						ee.setItemDescription(rs.getString("ItemDesc"));
						ee.setItemType(rs.getString("ItemType"));
						ee.setUnitPrice(rs.getString("UnitPrice"));
						ee.setWeight(rs.getString("Weight"));
						ee.setUom(rs.getString("UOM"));
						ee.setDcqty(rs.getString("dc_qty"));
						ee.setReturnqty(rs.getString("return_qty"));
						ee.setRemqty(rs.getString("rem_qty"));
						return ee;
					}

				});
	}

	public void dcReturnItems(EditListEntity elt) {
		String sql = "UPDATE cdeasia_deliverychalan set return_qty= '"+elt.getReturnqty()+"',rem_qty = '"+elt.getRemqty()+"',return_status = 'Returned',"
				+ "updated_at = NOW() where deliverychallan_no = '"+elt.getDeliverychallan()+"' and ItemCode = '"+elt.getItemCode()+"'";
			jdbcTemplate.update(sql);
	}

	public List<VendorPOListEntity> vendorPOstatList(String vcode) {
		return jdbcTemplate.query("select * from  cdeasia_purchase.cdeasia_vendorpomap vpo where vpo.VendorCode='" + vcode + "' group by PONumber and"
				+ " (VendorAcceptanceStatus = 'Accept' OR VendorAcceptanceStatus = 'Reject')",
			new RowMapper<VendorPOListEntity>() {

				public VendorPOListEntity mapRow(ResultSet rs, int row) throws SQLException {
					VendorPOListEntity obj = new VendorPOListEntity();
					obj.setIndentID(rs.getString("IndentId"));
					
					obj.setProjectID(rs.getString("ProjectId"));
					obj.setPoNumber(rs.getString("PONumber"));
					obj.setStatus(rs.getString("VendorAcceptanceStatus"));
					return obj;
				}

			});
	}

	public List<VendorPOListEntity> vendorPoStatItemDetails(VendorPOListEntity vendorPOListEntity) {
		return jdbcTemplate.query("select * from cdeasia_item_master itm, cdeasia_purchase.cdeasia_vendorpomap vpo "
				+ "where itm.ItemCode=vpo.ItemCode AND vpo.VendorAcceptanceStatus IS NOT NULL and vpo.PONumber='" + vendorPOListEntity.getPoNumber() + "'",
				new RowMapper<VendorPOListEntity>() {

					public VendorPOListEntity mapRow(ResultSet rs, int row) throws SQLException {
						VendorPOListEntity obj = new VendorPOListEntity();
						obj.setItemCode(rs.getString("ItemCode"));
						obj.setItemDescription(rs.getString("ItemDesc"));
						obj.setItemType(rs.getString("ItemType"));
						obj.setUnitPrice(rs.getString("UnitPrice"));
						obj.setUom(rs.getString("UOM"));
						obj.setWeight(rs.getString("Weight"));
						obj.setPoqty(rs.getString("ArvlQty"));
						return obj;
					}

				});
	}
	
	
		// API RELATED QUERIES

			//Item List
			public List<VendorPricingItemList> itemlistapi(String key) {
				String sql = "select itm.* from cdeasia_ims.cdeasia_item_master itm where itm.ItemCode IN(" + key + ")";
				System.out.println(sql);
				return jdbcTemplate.query("select * from cdeasia_ims.cdeasia_item_master itm where itm.ItemCode IN(" + key + ")",
						new RowMapper<VendorPricingItemList>() {
		
							public VendorPricingItemList mapRow(ResultSet rs, int row) throws SQLException {
								VendorPricingItemList ee = new VendorPricingItemList();
								ee.setItemCode(rs.getString("ItemCode"));
								ee.setItemDescription(rs.getString("ItemDesc"));
								ee.setItemType(rs.getString("ItemType"));
								ee.setUnitPrice(rs.getString("UnitPrice"));
								ee.setAvlqty(rs.getString("AvlQty"));
								ee.setCategory(rs.getString("Category"));
								ee.setProjectid(rs.getString("ProjectId"));
								ee.setIndqty(rs.getString("IndQty"));
								ee.setLocation(rs.getString("Location"));
								ee.setTaxpercentage(rs.getString("TaxPercentage"));
								ee.setTaxtype(rs.getString("TaxType"));
								ee.setDrawing(rs.getString("drawing"));
								ee.setSubassemblydesign(rs.getString("subassembly_design"));
								return ee;
							}
		
						});
			}
			
			//Item Related
			public List<VendorPricingItemList> itemlistapiall() {
				String sql = "select * from cdeasia_item_master";
				System.out.println(sql);
				return jdbcTemplate.query("select * from cdeasia_ims.cdeasia_item_master itm ",
						new RowMapper<VendorPricingItemList>() {

							public VendorPricingItemList mapRow(ResultSet rs, int row) throws SQLException {
								VendorPricingItemList ee = new VendorPricingItemList();
								ee.setItemCode(rs.getString("ItemCode"));
								ee.setItemDescription(rs.getString("ItemDesc"));
								ee.setItemType(rs.getString("ItemType"));
								ee.setUnitPrice(rs.getString("UnitPrice"));
								ee.setAvlqty(rs.getString("AvlQty"));
								ee.setCategory(rs.getString("Category"));
								ee.setProjectid(rs.getString("ProjectId"));
								ee.setIndqty(rs.getString("IndQty"));
								ee.setLocation(rs.getString("Location"));
								ee.setTaxpercentage(rs.getString("TaxPercentage"));
								ee.setTaxtype(rs.getString("TaxType"));
								ee.setDrawing(rs.getString("drawing"));
								ee.setSubassemblydesign(rs.getString("subassembly_design"));
								return ee;
							}

						});
			}
			
			//Indent Related
			public List<IndentList> indentlistapi(String str) {
				String sql = "select it.* from cdeasia_ims.cdeasia_indent it where it.IndentId IN(" + str + ") AND it.IndQty <> 0";
				System.out.println(sql);
				return jdbcTemplate.query("select * from cdeasia_ims.cdeasia_indent it where it.IndentId IN(" + str + ") AND it.IndQty <> 0",
						new RowMapper<IndentList>() {

							public IndentList mapRow(ResultSet rs, int row) throws SQLException {
								IndentList ie = new IndentList();
								ie.setIndentid(rs.getString("IndentId"));
								ie.setProjectid(rs.getString("ProjectId"));
								ie.setKitid(rs.getString("KitId"));
								ie.setBomtype(rs.getString("BOMType"));
								ie.setItemcode(rs.getString("ItemCode"));
								ie.setItemdesc(rs.getString("ItemDesc"));
								ie.setIndqty(rs.getString("IndQty"));
								ie.setRaisedby(rs.getString("RaisedBy"));
								ie.setRaisedon(rs.getString("RaisedOn"));
								ie.setStatus(rs.getString("Status"));
								ie.setCreatedat(rs.getString("created_at"));
								
								return ie;
							}

						});
			}
			
			//Indent List
			public List<IndentList> indentlistapiall() {
				String sql = "select it.* cdeasia_ims.from cdeasia_indent WHERE IndQty <> 0";
				System.out.println(sql);
				return jdbcTemplate.query("select * from cdeasia_ims.cdeasia_indent WHERE IndQty <> 0",
						new RowMapper<IndentList>() {

							public IndentList mapRow(ResultSet rs, int row) throws SQLException {
								IndentList ie = new IndentList();
								ie.setIndentid(rs.getString("IndentId"));
								ie.setProjectid(rs.getString("ProjectId"));
								ie.setKitid(rs.getString("KitId"));
								ie.setBomtype(rs.getString("BOMType"));
								ie.setItemcode(rs.getString("ItemCode"));
								ie.setItemdesc(rs.getString("ItemDesc"));
								ie.setIndqty(rs.getString("IndQty"));
								ie.setRaisedby(rs.getString("RaisedBy"));
								ie.setRaisedon(rs.getString("RaisedOn"));
								ie.setStatus(rs.getString("Status"));
								ie.setCreatedat(rs.getString("created_at"));
								
								return ie;
							}

						});
			}
			
			//Indent Check
			public List<IndentList> checkitemByIndent(IndentList indentlist) {
				String sql = "select * from cdeasia_ims.cdeasia_indent where IndentId = '"+indentlist.getIndentid()+"' and ProjectId = '"+indentlist.getProjectid()+"' "
						+ "AND ItemCode= '"+indentlist.getItemcode()+"'";
				System.out.println(sql);
				return jdbcTemplate.query("select * from cdeasia_ims.cdeasia_indent where IndentId = '"+indentlist.getIndentid()+"' and ProjectId = '"+indentlist.getProjectid()+"' "
						+ "AND ItemCode= '"+indentlist.getItemcode()+"'",
						new RowMapper<IndentList>() {

							public IndentList mapRow(ResultSet rs, int row) throws SQLException {
								IndentList ie = new IndentList();
								ie.setIndentid(rs.getString("IndentId"));
								ie.setProjectid(rs.getString("ProjectId"));
								ie.setKitid(rs.getString("KitId"));
								ie.setBomtype(rs.getString("BOMType"));
								ie.setItemcode(rs.getString("ItemCode"));
								ie.setItemdesc(rs.getString("ItemDesc"));
								ie.setIndqty(rs.getString("IndQty"));
								ie.setRaisedby(rs.getString("RaisedBy"));
								ie.setRaisedon(rs.getString("RaisedOn"));
								ie.setStatus(rs.getString("Status"));
								ie.setCreatedat(rs.getString("created_at"));
								
								return ie;
							}

						});
			}
			
			//Create Indent
			public void createnewindent(IndentList indentlist) {
				String sql = "insert into cdeasia_ims.cdeasia_indent set  IndentId='"+indentlist.getIndentid()+"' ,ProjectId='"+indentlist.getProjectid()+"' , "
						+ "KitId='"+indentlist.getKitid()+"',kittitle='"+indentlist.getKittitle()+"',kitdescription='"+indentlist.getKitdescription()+"',"
						+ "BOMType='"+indentlist.getBomtype()+"' , ItemCode='"+indentlist.getItemcode()+"' ,ItemDesc='"+indentlist.getItemdesc()+"' ,"
						+ " IndQty='"+indentlist.getIndqty()+"' ,RaisedBy='"+indentlist.getRaisedby()+"',RaisedOn='"+indentlist.getRaisedon()+"' ";
					jdbcTemplate.update(sql);
				
			}
			
			//Update Indent
			public void updatenewindent(IndentList indentlist) {
				String sql = "UPDATE cdeasia_ims.cdeasia_indent set  IndentId='"+indentlist.getIndentid()+"' ,ProjectId='"+indentlist.getProjectid()+"' , "
						+ "KitId='"+indentlist.getKitid()+"',kittitle='"+indentlist.getKittitle()+"',kitdescription='"+indentlist.getKitdescription()+"',"
						+ "BOMType='"+indentlist.getBomtype()+"' , ItemCode='"+indentlist.getItemcode()+"' ,ItemDesc='"+indentlist.getItemdesc()+"' ,"
						+ " IndQty='"+indentlist.getIndqty()+"' ,RaisedBy='"+indentlist.getRaisedby()+"',RaisedOn='"+indentlist.getRaisedon()+"' "
								+ "WHERE IndentId='"+indentlist.getIndentid()+"' AND ProjectId='"+indentlist.getProjectid()+"' AND ItemCode='"+indentlist.getItemcode()+"'";
					jdbcTemplate.update(sql);
				
			}  

			public List<LeadsList> matureleads() {
				String sql = "SELECT cld.*,cls.*,clm.*,clc.*,clm.created_at as clm_created "
						+ "FROM `cdeasia_lead_detail` cld,cdeasia_lead_status cls,cdeasia_lead_master clm,"
						+ "cdeasia_lead_to_customer clc "
						+ "WHERE clm.lead_id = cld.lead_id AND clm.lead_id = cls.lead_id AND clm.lead_id = clc.lead_id "
						+ "AND cls.lead_status = 'Qualified'";
				System.out.println(sql);
				return jdbcTemplate.query("SELECT cld.*,cls.*,clm.*,clc.*,clm.created_at as clm_created "
						+ "FROM `cdeasia_lead_detail` cld,cdeasia_lead_status cls,cdeasia_lead_master clm,"
						+ "cdeasia_lead_to_customer clc "
						+ "WHERE clm.lead_id = cld.lead_id AND clm.lead_id = cls.lead_id AND clm.lead_id = clc.lead_id "
						+ "AND cls.lead_status = 'Qualified'",
						new RowMapper<LeadsList>() {

							public LeadsList mapRow(ResultSet rs, int row) throws SQLException {
								LeadsList ll = new LeadsList();
								ll.setLeadid(rs.getString("lead_id"));
								ll.setCustomerid(rs.getString("customer_id"));
								ll.setLeadname(rs.getString("name"));
								ll.setState(rs.getString("state"));
								ll.setCountry(rs.getString("country"));
								ll.setLocation(rs.getString("location"));
								ll.setLatitude(rs.getString("latitude"));
								ll.setLongitude(rs.getString("longitude"));
								ll.setContactno(rs.getString("contact_no"));
								ll.setPannumber(rs.getString("pan_no"));
								ll.setGstin(rs.getString("gstin"));
								ll.setUid(rs.getString("uid"));
								ll.setPonumber(rs.getString("po_number"));
								ll.setPoissuedate(rs.getString("po_issue_date"));
								ll.setCuststatus(rs.getString("cust_status"));
								ll.setShortrequirement(rs.getString("short_requirement"));
								ll.setFullrequirement(rs.getString("full_requirement"));
								ll.setLeadcreatedon(rs.getString("clm_created"));
								ll.setPlanttype(rs.getString("plant_type"));
								ll.setCapacity(rs.getString("capacity"));
								ll.setLeadnote(rs.getString("lead_note"));
								//ll.setSampleid(rs.getString("sample_id"));
								//ll.setSamplecollected(rs.getString("sample_collected"));
								//ll.setSamplecollectedon(rs.getString("clsd_created"));
								//ll.setSampletype(rs.getString("sample_type"));
								//ll.setSampledesc(rs.getString("sample_desc"));
								ll.setLeadstatus(rs.getString("lead_status"));
								ll.setUploadfile(rs.getString("upload_file"));
								ll.setFilepath(rs.getString("file_path"));
								
								return ll;
							}
						});


			}

			public List<EmployeeList> employeelist() {
				String sql = "SELECT cm.*,cdept.departmentName,cdesig.designationName,cl.levelName "
						+ "FROM cdeasia_hrms.cde_employee_master cm,cdeasia_hrms.cde_designation_master cdesig,cdeasia_hrms.cde_department_master cdept,"
						+ "cdeasia_hrms.cde_level_master cl "
						+ "WHERE cm.designationID = cdesig.designationID AND cm.levelId = cl.levelId AND cm.departmentID = cdept.departmentID";
				System.out.println(sql);
				return jdbcTemplate.query("SELECT cm.*,cdept.departmentName,cdesig.designationName,cl.levelName "
						+ "FROM cdeasia_hrms.cde_employee_master cm,cdeasia_hrms.cde_designation_master cdesig,cdeasia_hrms.cde_department_master cdept,"
						+ "cdeasia_hrms.cde_level_master cl "
						+ "WHERE cm.designationID = cdesig.designationID AND cm.levelId = cl.levelId AND cm.departmentID = cdept.departmentID",
						new RowMapper<EmployeeList>() {

							public EmployeeList mapRow(ResultSet rs, int row) throws SQLException {
								EmployeeList el = new EmployeeList();
								el.setEmployeeid(rs.getString("EmployeeID"));
								el.setEmployeename(rs.getString("EmployeeName"));
								el.setDob(rs.getString("dob"));
								el.setGender(rs.getString("gender"));
								el.setOfficialemail(rs.getString("officialEmail"));
								el.setContactno1(rs.getString("contactNO1"));
								el.setContactno2(rs.getString("contactNO2"));
								el.setDoj(rs.getString("dateOfJoining"));
								el.setDor(rs.getString("daateOfResignation"));
								el.setAddress(rs.getString("address"));
								el.setEmptype(rs.getString("EmpType"));
								el.setDesgnationid(rs.getString("designationID"));
								el.setDesignationname(rs.getString("designationName"));
								el.setDepartmentid(rs.getString("departmentID"));
								el.setDepartmentname(rs.getString("departmentName"));
								el.setLevelid(rs.getString("levelId"));
								el.setLevelname(rs.getString("levelName"));
								
								return el;
							}
						});
			}

			public void createnewproject(LeadsList leadlist) {
				String sql = "insert into cdeasia_lead_project set projectName = '"+leadlist.getProjectname()+"' , projectCode = '"+leadlist.getProjectcode()+"',"
						+ "plantId = '"+leadlist.getPlantid()+"',plantType='"+leadlist.getPlanttype()+"',projectDtls = '"+leadlist.getProjectdetails()+"',"
						+ "projectSpecification = '"+leadlist.getProjectspecification()+"',additionalDtls='"+leadlist.getAdditionaldetails()+"',"
						+ "specialFeaturesOfPlant = '"+leadlist.getSpecialfeaturesofplant()+"',purposeOfPlant = '"+leadlist.getPurposeofplant()+"',"
						+ "startDate = '"+leadlist.getStartdate()+"',lead_id = '"+leadlist.getLeadid()+"',cust_id='"+leadlist.getCustomerid()+"'";
					jdbcTemplate.update(sql);
			}

			public List<ItemWorkEntity> itemworkmaster() {
				String sql = "Select * from cdeasia_ims.cdeasia_item_work_master";
				System.out.println(sql);
				return jdbcTemplate.query("Select * from cdeasia_ims.cdeasia_item_work_master",
						new RowMapper<ItemWorkEntity>() {

							public ItemWorkEntity mapRow(ResultSet rs, int row) throws SQLException {
								ItemWorkEntity ie = new ItemWorkEntity();
								ie.setWorkmasterid(rs.getString("work_master_id"));
								ie.setDescription(rs.getString("description"));
								ie.setStatus(rs.getString("status"));
								ie.setCreatedat(rs.getString("created_at"));
								
								return ie;
							}
						});
			}
	


}
