package com.cde.crm.repository;

import java.util.List;

import com.cde.crm.entity.EditListEntity;
import com.cde.crm.entity.EditPriceEntity;
import com.cde.crm.entity.EmployeeList;
import com.cde.crm.entity.IndentList;
import com.cde.crm.entity.LeadsList;
import com.cde.crm.entity.VendorPOListEntity;
import com.cde.crm.entity.VendorPricingItemList;

public interface VendorPricingListInterface {

	public List<VendorPricingItemList> vendorPricingItemList(String vendorCode);
	public int updateVendorPrice(EditPriceEntity editPriceEntity);
	public boolean isvendorcodeExists(String vendorCode);
	public List<EditListEntity> itemlist(String itemCode,String vcode);
	public List<VendorPOListEntity> vendorPOList(String vendorCode);
	public List<VendorPOListEntity> vendorPoItemDetails(VendorPOListEntity vendorPOListEntity);
	public int updateVendorStatus(VendorPOListEntity vendorPOListEntity);
	
	public List<VendorPOListEntity> vendorPORcv(String pass,String strt_date,String end_date);
	public List<VendorPOListEntity> vendorPOAccept(String pass,String strt_date,String end_date);
	public List<VendorPOListEntity> vendorPOReject(String pass,String strt_date,String end_date);
	public List<VendorPOListEntity> vendorPOListLatest(String pass,String strt_date,String end_date);
	public List<VendorPricingItemList> vendorPricingItemListLatest(String pass,String strt_date,String end_date);
	
	public List<VendorPOListEntity> vendorDCList(String vendorCode);
	public List<EditListEntity> dcitemlist(VendorPOListEntity vpe, String vcode);
	public void dcReturnItems(EditListEntity elt);
	public List<VendorPOListEntity> vendorPOstatList(String vcode);
	public List<VendorPOListEntity> vendorPoStatItemDetails(VendorPOListEntity vendorPOListEntity);
	
	
	//API LIST
	public List<VendorPricingItemList> itemlistapi(String key);
	public List<VendorPricingItemList> itemlistapiall();
	public List<IndentList> indentlistapi(String str);
	public List<IndentList> indentlistapiall();	
	public void createindent(String indentid, String projectid, String kitid,String kittitle,String kitdesc, String bomtype, String itemcode,
			String itemdesc, String indqty, String raisedby, String raisedon);
	public List<IndentList> checkitem(String items, String indentid, String projectid);
	public void updateindent(String indentid, String projectid, String kit, String title, String desc, String bomtype,
			String items, String itdesc, String qty, String raisedby, String raisedon);
	public void createnewindent(IndentList indentlist);
	public List<IndentList> checkitemByIndent(IndentList indentlist);
	public void updatenewindent(IndentList indentlist);
	public List<LeadsList> matureleads();
	public List<EmployeeList> employeelist();
	public void createnewproject(LeadsList leadlist);
	
}
