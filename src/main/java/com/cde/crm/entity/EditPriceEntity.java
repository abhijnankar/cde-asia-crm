package com.cde.crm.entity;

public class EditPriceEntity {
	private String vendorPrice;
	private String edititemcode;
	private String vCode;
	private String leadtime;
	private String uploadfile;
	private String filepath;
	
	
	public String getLeadtime() {
		return leadtime;
	}
	public void setLeadtime(String leadtime) {
		this.leadtime = leadtime;
	}
	public String getUploadfile() {
		return uploadfile;
	}
	public void setUploadfile(String uploadfile) {
		this.uploadfile = uploadfile;
	}
	public String getvCode() {
		return vCode;
	}
	public void setvCode(String vCode) {
		this.vCode = vCode;
	}
	public String getEdititemcode() {
		return edititemcode;
	}
	public void setEdititemcode(String edititemcode) {
		this.edititemcode = edititemcode;
	}
	public String getVendorPrice() {
		return vendorPrice;
	}
	public void setVendorPrice(String vendorPrice) {
		this.vendorPrice = vendorPrice;
	}
	public String getFilepath() {
		return filepath;
	}
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	
	

}
