package com.cde.crm.entity;

public class VendorPOListEntity {

	private String indentID;
	private String projectID;
	private String poNumber;
	private String itemCode;
	private String itemDescription;
	private String itemType;
	private String unitPrice;
	private String weight;
	private String uom;
	
	private String status;
	private String poStatus;
	
	private String porcv;
	private String poaccept;
	private String poreject;
	private String ponumber;
	private String poqty;
	
	private String deliverycharge;
	private String packingcharge;
	private String miscellaneouscharge;
	private String comments;
	private String uploadfile;
	
	private String deliverychallan;
	
	
	public String getDeliverychallan() {
		return deliverychallan;
	}
	public void setDeliverychallan(String deliverychallan) {
		this.deliverychallan = deliverychallan;
	}
	public String getDeliverycharge() {
		return deliverycharge;
	}
	public void setDeliverycharge(String deliverycharge) {
		this.deliverycharge = deliverycharge;
	}
	public String getPackingcharge() {
		return packingcharge;
	}
	public void setPackingcharge(String packingcharge) {
		this.packingcharge = packingcharge;
	}
	public String getMiscellaneouscharge() {
		return miscellaneouscharge;
	}
	public void setMiscellaneouscharge(String miscellaneouscharge) {
		this.miscellaneouscharge = miscellaneouscharge;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getUploadfile() {
		return uploadfile;
	}
	public void setUploadfile(String uploadfile) {
		this.uploadfile = uploadfile;
	}
	public String getPorcv() {
		return porcv;
	}
	public void setPorcv(String porcv) {
		this.porcv = porcv;
	}
	public String getPoaccept() {
		return poaccept;
	}
	public void setPoaccept(String poaccept) {
		this.poaccept = poaccept;
	}
	public String getPoreject() {
		return poreject;
	}
	public void setPoreject(String poreject) {
		this.poreject = poreject;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPoStatus() {
		return poStatus;
	}
	public void setPoStatus(String poStatus) {
		this.poStatus = poStatus;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getItemDescription() {
		return itemDescription;
	}
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	public String getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public String getIndentID() {
		return indentID;
	}
	public void setIndentID(String indentID) {
		this.indentID = indentID;
	}
	public String getProjectID() {
		return projectID;
	}
	public void setProjectID(String projectID) {
		this.projectID = projectID;
	}
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public String getPonumber() {
		return ponumber;
	}
	public void setPonumber(String ponumber) {
		this.ponumber = ponumber;
	}
	public String getPoqty() {
		return poqty;
	}
	public void setPoqty(String poqty) {
		this.poqty = poqty;
	}
	
	
	
}
