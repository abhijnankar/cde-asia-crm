package com.cde.crm.entity;

public class ItemWorkEntity {

	private String workmasterid;
	private String description;
	private String status;
	private String createdat;
	public String getWorkmasterid() {
		return workmasterid;
	}
	public void setWorkmasterid(String workmasterid) {
		this.workmasterid = workmasterid;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedat() {
		return createdat;
	}
	public void setCreatedat(String createdat) {
		this.createdat = createdat;
	}
	
}
