package com.cde.crm.entity;

public class EditListEntity {
	
	private String itemCode;
	private String itemDescription;
	private String itemType;
	private String unitPrice;
	private String vendorprice;
	private String leadtime;
	
	private String weight;
	private String uom;
	private String dcqty;
	private String returnqty;
	private String remqty;
	private String deliverychallan;
	private String uploadfile;
	private String filepath;
	
	
	
	public String getUploadfile() {
		return uploadfile;
	}
	public void setUploadfile(String uploadfile) {
		this.uploadfile = uploadfile;
	}
	public String getFilepath() {
		return filepath;
	}
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	public String getDcqty() {
		return dcqty;
	}
	public void setDcqty(String dcqty) {
		this.dcqty = dcqty;
	}
	public String getReturnqty() {
		return returnqty;
	}
	public void setReturnqty(String returnqty) {
		this.returnqty = returnqty;
	}
	public String getRemqty() {
		return remqty;
	}
	public void setRemqty(String remqty) {
		this.remqty = remqty;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public String getVendorprice() {
		return vendorprice;
	}
	public void setVendorprice(String vendorprice) {
		this.vendorprice = vendorprice;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getItemDescription() {
		return itemDescription;
	}
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	public String getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}
	public String getLeadtime() {
		return leadtime;
	}
	public void setLeadtime(String leadtime) {
		this.leadtime = leadtime;
	}
	public String getDeliverychallan() {
		return deliverychallan;
	}
	public void setDeliverychallan(String deliverychallan) {
		this.deliverychallan = deliverychallan;
	}

}
