package com.cde.crm.entity;

public class LeadsList {
	
	private String leadid;
	private String leadname;
	private String location;
	private String contactno;
	private String email;
	private String shortrequirement;
	private String leadcreatedon;
	private String planttype;
	private String capacity;
	private String samplecollected;
	private String fullrequirement;
	private String leadnote;
	private String sampleid;
	private String sampletype;
	private String sampledesc;
	private String samplecollectedon;
	private String leadstatus;
	private String customerid;
	private String pannumber;
	private String gstin;
	private String uid;
	private String ponumber;
	private String poissuedate;
	private String uploadfile;
	private String filepath;
	private String custstatus;
	private String plantid;
	private String country;
	private String state;
	private String latitude;
	private String longitude;
	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	private String projectname;
	private String projectcode;
	private String projectdetails;
	private String projectspecification;
	private String additionaldetails;
	private String specialfeaturesofplant;
	private String purposeofplant;
	private String startdate;
	
	
	
	
	public String getPlantid() {
		return plantid;
	}
	public void setPlantid(String plantid) {
		this.plantid = plantid;
	}
	public String getProjectname() {
		return projectname;
	}
	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}
	public String getProjectcode() {
		return projectcode;
	}
	public void setProjectcode(String projectcode) {
		this.projectcode = projectcode;
	}
	public String getProjectdetails() {
		return projectdetails;
	}
	public void setProjectdetails(String projectdetails) {
		this.projectdetails = projectdetails;
	}
	public String getProjectspecification() {
		return projectspecification;
	}
	public void setProjectspecification(String projectspecification) {
		this.projectspecification = projectspecification;
	}
	public String getAdditionaldetails() {
		return additionaldetails;
	}
	public void setAdditionaldetails(String additionaldetails) {
		this.additionaldetails = additionaldetails;
	}
	public String getSpecialfeaturesofplant() {
		return specialfeaturesofplant;
	}
	public void setSpecialfeaturesofplant(String specialfeaturesofplant) {
		this.specialfeaturesofplant = specialfeaturesofplant;
	}
	public String getPurposeofplant() {
		return purposeofplant;
	}
	public void setPurposeofplant(String purposeofplant) {
		this.purposeofplant = purposeofplant;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getLeadid() {
		return leadid;
	}
	public void setLeadid(String leadid) {
		this.leadid = leadid;
	}
	public String getLeadname() {
		return leadname;
	}
	public void setLeadname(String leadname) {
		this.leadname = leadname;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getContactno() {
		return contactno;
	}
	public void setContactno(String contactno) {
		this.contactno = contactno;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getShortrequirement() {
		return shortrequirement;
	}
	public void setShortrequirement(String shortrequirement) {
		this.shortrequirement = shortrequirement;
	}
	public String getLeadcreatedon() {
		return leadcreatedon;
	}
	public void setLeadcreatedon(String leadcreatedon) {
		this.leadcreatedon = leadcreatedon;
	}
	public String getPlanttype() {
		return planttype;
	}
	public void setPlanttype(String planttype) {
		this.planttype = planttype;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	public String getSamplecollected() {
		return samplecollected;
	}
	public void setSamplecollected(String samplecollected) {
		this.samplecollected = samplecollected;
	}
	public String getFullrequirement() {
		return fullrequirement;
	}
	public void setFullrequirement(String fullrequirement) {
		this.fullrequirement = fullrequirement;
	}
	public String getLeadnote() {
		return leadnote;
	}
	public void setLeadnote(String leadnote) {
		this.leadnote = leadnote;
	}
	public String getSampleid() {
		return sampleid;
	}
	public void setSampleid(String sampleid) {
		this.sampleid = sampleid;
	}
	public String getSampletype() {
		return sampletype;
	}
	public void setSampletype(String sampletype) {
		this.sampletype = sampletype;
	}
	public String getSamplecollectedon() {
		return samplecollectedon;
	}
	public void setSamplecollectedon(String samplecollectedon) {
		this.samplecollectedon = samplecollectedon;
	}
	public String getLeadstatus() {
		return leadstatus;
	}
	public void setLeadstatus(String leadstatus) {
		this.leadstatus = leadstatus;
	}
	public String getCustomerid() {
		return customerid;
	}
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	public String getPannumber() {
		return pannumber;
	}
	public void setPannumber(String pannumber) {
		this.pannumber = pannumber;
	}
	public String getGstin() {
		return gstin;
	}
	public void setGstin(String gstin) {
		this.gstin = gstin;
	}
	public String getPonumber() {
		return ponumber;
	}
	public void setPonumber(String ponumber) {
		this.ponumber = ponumber;
	}
	public String getPoissuedate() {
		return poissuedate;
	}
	public void setPoissuedate(String poissuedate) {
		this.poissuedate = poissuedate;
	}
	public String getUploadfile() {
		return uploadfile;
	}
	public void setUploadfile(String uploadfile) {
		this.uploadfile = uploadfile;
	}
	public String getFilepath() {
		return filepath;
	}
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	public String getCuststatus() {
		return custstatus;
	}
	public void setCuststatus(String custstatus) {
		this.custstatus = custstatus;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getSampledesc() {
		return sampledesc;
	}
	public void setSampledesc(String sampledesc) {
		this.sampledesc = sampledesc;
	}

}
