package com.cde.crm.entity;

public class IndentList {
	private String indentid;
	private String projectid;
	private String kitid;
	private String bomtype;
	private String itemcode;
	private String itemdesc;
	private String indqty;
	private String Status;
	private String raisedby;
	private String kittitle;
	private String kitdescription;
	
	
	public String getKittitle() {
		return kittitle;
	}
	public void setKittitle(String kittitle) {
		this.kittitle = kittitle;
	}
	public String getKitdescription() {
		return kitdescription;
	}
	public void setKitdescription(String kitdescription) {
		this.kitdescription = kitdescription;
	}
	public String getIndentid() {
		return indentid;
	}
	public void setIndentid(String indentid) {
		this.indentid = indentid;
	}
	public String getProjectid() {
		return projectid;
	}
	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}
	public String getKitid() {
		return kitid;
	}
	public void setKitid(String kitid) {
		this.kitid = kitid;
	}
	public String getBomtype() {
		return bomtype;
	}
	public void setBomtype(String bomtype) {
		this.bomtype = bomtype;
	}
	public String getItemcode() {
		return itemcode;
	}
	public void setItemcode(String itemcode) {
		this.itemcode = itemcode;
	}
	public String getItemdesc() {
		return itemdesc;
	}
	public void setItemdesc(String itemdesc) {
		this.itemdesc = itemdesc;
	}
	public String getIndqty() {
		return indqty;
	}
	public void setIndqty(String indqty) {
		this.indqty = indqty;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getRaisedby() {
		return raisedby;
	}
	public void setRaisedby(String raisedby) {
		this.raisedby = raisedby;
	}
	public String getRaisedon() {
		return raisedon;
	}
	public void setRaisedon(String raisedon) {
		this.raisedon = raisedon;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public String getCreatedat() {
		return createdat;
	}
	public void setCreatedat(String createdat) {
		this.createdat = createdat;
	}
	private String raisedon;
	private String createdby;
	private String createdat;
}
