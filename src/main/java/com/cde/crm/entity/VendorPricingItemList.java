package com.cde.crm.entity;

public class VendorPricingItemList {

	private String itemCode;
	private String itemDescription;
	private String itemType;
	private String unitPrice;
	private String weight;
	private String uom;
	private String vendorprice;
	private int code;
	private String avlqty;
	private String indqty;
	private String projectid;
	private String category;
	private String itemtype;
	private String taxtype;
	private String taxpercentage;
	private String location;
	
	private String leadtime;
	private String uploadfile;
	private String status;
	private String filepath;
	
	private String subassemblycode;
	private String subassemblydesc;
	private String subassemblydesign;
	private String drawing;

	public String getSubassemblycode() {
		return subassemblycode;
	}

	public void setSubassemblycode(String subassemblycode) {
		this.subassemblycode = subassemblycode;
	}

	public String getSubassemblydesc() {
		return subassemblydesc;
	}

	public void setSubassemblydesc(String subassemblydesc) {
		this.subassemblydesc = subassemblydesc;
	}

	public String getDrawing() {
		return drawing;
	}

	public void setDrawing(String drawing) {
		this.drawing = drawing;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public String getLeadtime() {
		return leadtime;
	}

	public void setLeadtime(String leadtime) {
		this.leadtime = leadtime;
	}

	public String getUploadfile() {
		return uploadfile;
	}

	public void setUploadfile(String uploadfile) {
		this.uploadfile = uploadfile;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAvlqty() {
		return avlqty;
	}

	public void setAvlqty(String avlqty) {
		this.avlqty = avlqty;
	}

	public String getIndqty() {
		return indqty;
	}

	public void setIndqty(String indqty) {
		this.indqty = indqty;
	}

	public String getProjectid() {
		return projectid;
	}

	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getItemtype() {
		return itemtype;
	}

	public void setItemtype(String itemtype) {
		this.itemtype = itemtype;
	}

	public String getTaxtype() {
		return taxtype;
	}

	public void setTaxtype(String taxtype) {
		this.taxtype = taxtype;
	}

	public String getTaxpercentage() {
		return taxpercentage;
	}

	public void setTaxpercentage(String taxpercentage) {
		this.taxpercentage = taxpercentage;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getVendorprice() {
		return vendorprice;
	}

	public void setVendorprice(String vendorprice) {
		this.vendorprice = vendorprice;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getSubassemblydesign() {
		return subassemblydesign;
	}

	public void setSubassemblydesign(String subassemblydesign) {
		this.subassemblydesign = subassemblydesign;
	}

}
