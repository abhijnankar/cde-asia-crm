package com.cde.crm.controller;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.cde.crm.entity.EditListEntity;
import com.cde.crm.entity.EditPriceEntity;
import com.cde.crm.entity.LoginEntityClass;
import com.cde.crm.entity.VendorPOListEntity;
import com.cde.crm.entity.VendorPricingItemList;
import com.cde.crm.repository.impl.CrmClass;
import com.cde.crm.repository.impl.VendorPricingListClass;

@Controller
@Scope("session")
public class HomeController {
	
	@Autowired
	VendorPricingListClass vendorPricingListClass;
	
	@Autowired
	CrmClass crmClass;
	
	
	String pass;
	/*
	 * @RequestMapping(value = "/", method = RequestMethod.GET) public ModelAndView
	 * homeview(Model m) { m.addAttribute("loginModel", new LoginEntityClass());
	 * return new ModelAndView("dashboard"); }
	 */

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String welcomeview(@ModelAttribute("loginModel") LoginEntityClass loginEntityClass, Model m,
			HttpServletRequest req, HttpServletResponse res) {
		String pass = loginEntityClass.getPassword();
		//String v;
		HttpSession session = req.getSession();
		session.setAttribute("pass", pass);
		session = req.getSession();
		session.setAttribute("vcode", loginEntityClass.getPassword());
		System.out.println(loginEntityClass.getPassword());
		
		String strtdt = "";
		String enddt = ""; 
		
		
		boolean status=vendorPricingListClass.isvendorcodeExists(pass);
		if (status) {
			List<VendorPOListEntity> porcv = vendorPricingListClass.vendorPORcv(pass,strtdt,enddt);
			List<VendorPOListEntity> poaccept = vendorPricingListClass.vendorPOAccept(pass,strtdt,enddt);
			List<VendorPOListEntity> poreject = vendorPricingListClass.vendorPOReject(pass,strtdt,enddt);
			
			List<VendorPOListEntity> vpolistlatest = vendorPricingListClass.vendorPOListLatest(pass,strtdt,enddt);
			List<VendorPricingItemList> pricinglistlatest = vendorPricingListClass.vendorPricingItemListLatest(pass,strtdt,enddt);
			
			List<VendorPricingItemList> pricinglist = vendorPricingListClass.vendorPricingItemList(pass);
			List<VendorPOListEntity> vpolist = vendorPricingListClass.vendorPOList(pass);
			m.addAttribute("pricinglist", pricinglist);
			m.addAttribute("vpolist", vpolist);
			m.addAttribute("porcv", porcv);
			m.addAttribute("poaccept", poaccept);
			m.addAttribute("poreject", poreject);
			m.addAttribute("vpolistlatest", vpolistlatest);
			m.addAttribute("pricinglistlatest", pricinglistlatest);
			m.addAttribute("viewPODetails", new VendorPOListEntity());
			m.addAttribute("fromdate",strtdt);
			m.addAttribute("todate",enddt);
			return "vendordashboard";
		} else {
			return "error";
		}
	}
	

	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public String dashboard(@ModelAttribute("loginModel") LoginEntityClass loginEntityClass,Model m,HttpServletRequest req, HttpServletResponse res) throws Exception {
		String pass = (String) req.getSession().getAttribute("pass");
		String strtdt = "";
		String enddt = ""; 
		
		List<VendorPOListEntity> porcv = vendorPricingListClass.vendorPORcv(pass,strtdt,enddt);
		List<VendorPOListEntity> poaccept = vendorPricingListClass.vendorPOAccept(pass,strtdt,enddt);
		List<VendorPOListEntity> poreject = vendorPricingListClass.vendorPOReject(pass,strtdt,enddt);
		
		List<VendorPOListEntity> vpolistlatest = vendorPricingListClass.vendorPOListLatest(pass,strtdt,enddt);
		List<VendorPricingItemList> pricinglistlatest = vendorPricingListClass.vendorPricingItemListLatest(pass,strtdt,enddt);
		
		List<VendorPricingItemList> pricinglist = vendorPricingListClass.vendorPricingItemList(pass);
		List<VendorPOListEntity> vpolist = vendorPricingListClass.vendorPOList(pass);
		m.addAttribute("pricinglist", pricinglist);
		m.addAttribute("vpolist", vpolist);
		m.addAttribute("porcv", porcv);
		m.addAttribute("poaccept", poaccept);
		m.addAttribute("poreject", poreject);
		m.addAttribute("vpolistlatest", vpolistlatest);
		m.addAttribute("pricinglistlatest", pricinglistlatest);
		m.addAttribute("viewPODetails", new VendorPOListEntity());
		m.addAttribute("fromdate",strtdt);
		m.addAttribute("todate",enddt);
		return "vendordashboard";
	}
	@RequestMapping(value = "/viewdashboard", method = RequestMethod.POST)
	public ModelAndView viewdashboard(@ModelAttribute("loginModel") LoginEntityClass loginEntityClass,Model m,
							HttpServletRequest req, HttpServletResponse res,
							@RequestParam("from_date") String frmdate,@RequestParam("to_date") String todate) throws Exception {
		String strreplaceString=frmdate.replace('/','-');
		String endreplaceString=todate.replace('/','-');
		
		
		
		/*
		 * String startDate = strreplaceString; SimpleDateFormat strtformatter = new
		 * SimpleDateFormat("dd-MM-yyyy"); Date strdate = (Date)
		 * strtformatter.parse(startDate); SimpleDateFormat strtnewFormat = new
		 * SimpleDateFormat("yyyy-MM-dd"); String strtdt =
		 * strtnewFormat.format(strdate);
		 */
		 
		 
		
		/*
		 * String endDate = endreplaceString; SimpleDateFormat endformatter = new
		 * SimpleDateFormat("dd-MM-yyyy"); Date enddate = (Date)
		 * endformatter.parse(endDate); SimpleDateFormat endnewFormat = new
		 * SimpleDateFormat("yyyy-MM-dd"); String enddt = endnewFormat.format(enddate);
		 */
		
		System.out.println(strreplaceString);
		System.out.println(endreplaceString);
		
		m.addAttribute("fromdate",strreplaceString);
		m.addAttribute("todate",endreplaceString);
		
		String pass = (String) req.getSession().getAttribute("pass");
		List<VendorPOListEntity> porcv = vendorPricingListClass.vendorPORcv(pass,strreplaceString,endreplaceString);
		List<VendorPOListEntity> poaccept = vendorPricingListClass.vendorPOAccept(pass,strreplaceString,endreplaceString);
		List<VendorPOListEntity> poreject = vendorPricingListClass.vendorPOReject(pass,strreplaceString,endreplaceString);
		
		List<VendorPOListEntity> vpolistlatest = vendorPricingListClass.vendorPOListLatest(pass,strreplaceString,endreplaceString);
		List<VendorPricingItemList> pricinglistlatest = vendorPricingListClass.vendorPricingItemListLatest(pass,strreplaceString,endreplaceString);
		
		List<VendorPricingItemList> pricinglist = vendorPricingListClass.vendorPricingItemList(pass);
		List<VendorPOListEntity> vpolist = vendorPricingListClass.vendorPOList(pass);
		m.addAttribute("pricinglist", pricinglist);
		m.addAttribute("vpolist", vpolist);
		m.addAttribute("porcv", porcv);
		m.addAttribute("poaccept", poaccept);
		m.addAttribute("poreject", poreject);
		m.addAttribute("vpolistlatest", vpolistlatest);
		m.addAttribute("pricinglistlatest", pricinglistlatest);
		m.addAttribute("viewPODetails", new VendorPOListEntity());
		
		
		return new ModelAndView("vendordashboard");
		
	}
	
	@RequestMapping(value = "/itemprice", method = RequestMethod.GET)
	public String itemmasterview(@ModelAttribute("loginModel") LoginEntityClass loginEntityClass,Model m,HttpServletRequest req, HttpServletResponse res) throws Exception {
		System.out.println(req.getSession().getAttribute("pass"));
		String vcode = (String) req.getSession().getAttribute("pass");
		//String name = (String) request.getSession().getAttribute("pass");
		List<VendorPricingItemList> pricinglist = vendorPricingListClass.vendorPricingItemList(vcode);
		m.addAttribute("pricinglist", pricinglist);
		
		System.out.println(vcode);
		return "itemprice";
	}
	
	@RequestMapping(value = "/polist", method = RequestMethod.GET)
	public String polist(@ModelAttribute("viewPODetails") VendorPOListEntity vpe,Model m,HttpServletRequest req, HttpServletResponse res) throws Exception {
		String vcode = (String) req.getSession().getAttribute("pass");
		List<VendorPOListEntity> vpolist = vendorPricingListClass.vendorPOList(vcode);
		List<VendorPOListEntity> postatlist = vendorPricingListClass.vendorPOstatList(vcode);
		
		m.addAttribute("vpolist", vpolist);
		m.addAttribute("postatlist", postatlist);
		
		System.out.println(vcode);
		return "polist";
	}
	

	@RequestMapping(value = "/editPrice", method = RequestMethod.POST)
	public ModelAndView editview(@RequestParam("itemCode") String itemCode, Model m,HttpServletRequest req, HttpServletResponse res) {
		
		String vcode = (String) req.getSession().getAttribute("pass");
		List<EditListEntity> editlist=vendorPricingListClass.itemlist(itemCode,vcode);
		m.addAttribute("editlist", editlist);
		return new ModelAndView("editvendorprice");
	}
	
	
	private static final String path ="F:/java workspace/CDEVendor/src/main/webapp/resources/downloads";
	//private static final String path ="C:/ApacheTomcat8.5.45/webapps/CDEVendor/resources/downloads";
	@RequestMapping(value = "/updateVendorPrice", method = RequestMethod.POST)
	public String updateprice(@RequestParam("vendorPrice") String vnPrice,@RequestParam("itmCode") String itemCode, Model m,@RequestParam("prev_file") String prevfile,
							@RequestParam("lead_time") String leadtime,@RequestParam CommonsMultipartFile file,HttpServletRequest req,HttpServletResponse res) throws IOException {
		
		HttpSession session = req.getSession();
		EditPriceEntity editPriceEntity=new EditPriceEntity();
		editPriceEntity.setVendorPrice(vnPrice);
		//String path=session.getServletContext().getRealPath("/");  
		String filename="";
		if(file.isEmpty()) {
			System.out.println(filename);
			if(prevfile.isEmpty()) {
				filename = "";
			}
			else {
				System.out.println(prevfile);
				filename = prevfile;
			}
		}
		else {
			System.out.println(file);
			filename=file.getOriginalFilename(); 			
	        System.out.println(path+" "+filename);
		 	byte barr[]=file.getBytes();  	         
	        BufferedOutputStream bout=new BufferedOutputStream(new FileOutputStream(path+"/"+filename));  
	        bout.write(barr);  
	        bout.flush();  
	        bout.close();
		}
		
	        
		String vCode=(String) session.getAttribute("pass");
		System.out.println(vCode);
		editPriceEntity.setvCode(vCode);
		editPriceEntity.setEdititemcode(itemCode);
		
		editPriceEntity.setUploadfile(filename);
		editPriceEntity.setLeadtime(leadtime);
		editPriceEntity.setFilepath(path);
		vendorPricingListClass.updateVendorPrice(editPriceEntity);
		//return "vendorpricing";
		return "redirect:/itemprice";
		//return vCode;
	}
	
	@RequestMapping(value = "/lotupdate", method = RequestMethod.POST)
	public String lotupdate(@RequestParam("vendorprice[]") String[] vnPrice,@RequestParam("itemCode[]") String[] itemCode, Model m,
							@RequestParam("leadtime[]") String[] leadtime,HttpServletRequest req,HttpServletResponse res) throws IOException {
		
		HttpSession session = req.getSession();
	        
		EditPriceEntity editPriceEntity=new EditPriceEntity();
		
		int i = 0;
		for (String itemcodes : itemCode) {	
			editPriceEntity.setVendorPrice(vnPrice[i]);
	        
			String vCode=(String) session.getAttribute("pass");
			System.out.println(vCode);
			editPriceEntity.setvCode(vCode);
			editPriceEntity.setEdititemcode(itemcodes);
			editPriceEntity.setLeadtime(leadtime[i]);
			vendorPricingListClass.updateVendorPrice(editPriceEntity);
			i++;
		}
		
		//return "vendorpricing";
		return "redirect:/itemprice";
		//return vCode;
	}
	
	@RequestMapping(value = "/viewPODetails", method = RequestMethod.POST)
	public ModelAndView viewPo(@ModelAttribute("viewPODetails") VendorPOListEntity vendorPOListEntity, Model m) {
		
		//showing bar 
		m.addAttribute("vendorPOListEntity", vendorPOListEntity);
		
		
		List<VendorPOListEntity> detailslist=vendorPricingListClass.vendorPoItemDetails(vendorPOListEntity);
		m.addAttribute("detailslist", detailslist);
		return new ModelAndView("poitemdetails");
	}
	@RequestMapping(value = "/viewPOstatDetails", method = RequestMethod.POST)
	public ModelAndView viewPOstatDetails(@ModelAttribute("viewPODetails") VendorPOListEntity vendorPOListEntity, Model m) {
		
		//showing bar 
		m.addAttribute("vendorPOListEntity", vendorPOListEntity);
		
		
		List<VendorPOListEntity> detailslist=vendorPricingListClass.vendorPoStatItemDetails(vendorPOListEntity);
		m.addAttribute("detailslist", detailslist);
		return new ModelAndView("postatitemdetails");
	}
	
	@RequestMapping(value = "/AcceptOrReject", method = RequestMethod.POST)
	public String StatusChange(@ModelAttribute("viewPODetails") VendorPOListEntity vpe,@RequestParam("status") String Status,
								@RequestParam("poNumber") String poNumber,@RequestParam("itemcodes[]") String[] itemcode,
								@RequestParam("delivery_charge") String deliverycharge,@RequestParam("packing_charge") String packingcharge,
								@RequestParam("miscellaneous_charge") String miscellaneouscharge,@RequestParam("comments") String comments,
								@RequestParam CommonsMultipartFile file,Model m,HttpServletRequest req) throws IOException {
		VendorPOListEntity vendorPOListEntity=new VendorPOListEntity();
		
		 	String filename=file.getOriginalFilename(); 
	        System.out.println(path+" "+filename);
	        byte barr[]=file.getBytes();  
	         
	        BufferedOutputStream bout=new BufferedOutputStream(new FileOutputStream(path+"/"+filename));  
	        bout.write(barr);  
	        bout.flush();  
	        bout.close();
		
		for (String itemcodes : itemcode) {	
			vendorPOListEntity.setStatus(Status);
			//vendorPOListEntity.setPoStatus(poNumber);
			vendorPOListEntity.setPonumber(poNumber);
			vendorPOListEntity.setDeliverycharge(deliverycharge);
			vendorPOListEntity.setPackingcharge(packingcharge);
			vendorPOListEntity.setMiscellaneouscharge(miscellaneouscharge);
			vendorPOListEntity.setComments(comments);
			vendorPOListEntity.setUploadfile(filename);
			vendorPOListEntity.setItemCode(itemcodes);
			HttpSession session = req.getSession();
			String vCode=(String) session.getAttribute("pass");
			System.out.println(vCode);			
			vendorPricingListClass.updateVendorStatus(vendorPOListEntity);
		}
		
		//return "vendorpricing";
//		List<VendorPOListEntity> vpolist = vendorPricingListClass.vendorPOList(vCode);
//		m.addAttribute("vpolist", vpolist);
//		return "polist";
		return "redirect:/polist";
	}
	@RequestMapping(value = "/dcitems", method = RequestMethod.GET)
	public String dcitems(@ModelAttribute("viewPODetails") VendorPOListEntity vpe,Model m,HttpServletRequest req, HttpServletResponse res) throws Exception {
		String vcode = (String) req.getSession().getAttribute("pass");
		List<VendorPOListEntity> vpolist = vendorPricingListClass.vendorDCList(vcode);
		m.addAttribute("vpolist", vpolist);
		
		System.out.println(vcode);
		return "dcitems";
	}
	@RequestMapping(value = "/dcitemdetails", method = RequestMethod.POST)
	public ModelAndView dcitemdetails(@ModelAttribute("viewPODetails") VendorPOListEntity vpe,
										@RequestParam("deliverychallan") String deliverychallan,Model m,HttpServletRequest req, HttpServletResponse res) {
		vpe.setDeliverychallan(deliverychallan);
		
		String vcode = (String) req.getSession().getAttribute("pass");
		List<EditListEntity> dcitemlist=vendorPricingListClass.dcitemlist(vpe,vcode);
		m.addAttribute("dcitemlist", dcitemlist);
		m.addAttribute("deliverychallan",deliverychallan);
		return new ModelAndView("dcitemdetails");
	}

	@RequestMapping(value = "/dcreturnitems", method = RequestMethod.POST)
	public String dcreturnitems(@ModelAttribute("viewPODetails") EditListEntity el,@RequestParam("dcqnty[]") String[] dcqty,
								@RequestParam("returnqnty[]") String[] returnqty,@RequestParam("itemcodes[]") String[] itemcode,
								@RequestParam("remqnty[]") String[] remqty,@RequestParam("dcnumber") String dcnumber
								,Model m,HttpServletRequest req) throws IOException {
		EditListEntity elt=new EditListEntity();
		
		
		int i=0;
		for (String itemcodes : itemcode) {	
			//vendorPOListEntity.setPoStatus(poNumber);
			int rem = Integer.parseInt(dcqty[i])-Integer.parseInt(returnqty[i]);
			elt.setItemCode(itemcodes);
			elt.setDcqty(dcqty[i]);
			elt.setReturnqty(returnqty[i]);
			remqty[i] = Integer.toString(rem);
			elt.setRemqty(remqty[i]);
			elt.setDeliverychallan(dcnumber);
			HttpSession session = req.getSession();
			String vCode=(String) session.getAttribute("pass");
			System.out.println(vCode);			
			vendorPricingListClass.dcReturnItems(elt);
			i++;
		}
		return "redirect:/dcitems";
	}
	

}
