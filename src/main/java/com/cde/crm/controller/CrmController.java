package com.cde.crm.controller;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.cde.crm.entity.EditListEntity;
import com.cde.crm.entity.EditPriceEntity;
import com.cde.crm.entity.LoginEntityClass;
import com.cde.crm.entity.VendorPOListEntity;
import com.cde.crm.entity.VendorPricingItemList;
import com.cde.crm.repository.impl.CrmClass;
import com.cde.crm.repository.impl.VendorPricingListClass;

@Controller
@Scope("session")
public class CrmController {
	
	@Autowired
	VendorPricingListClass vendorPricingListClass;
	
	@Autowired
	CrmClass crmClass;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView homeview(Model m) {
		m.addAttribute("loginModel", new LoginEntityClass());
		return new ModelAndView("dashboard");
	}
	
	@RequestMapping(value = "/leads", method = RequestMethod.GET)
	public ModelAndView leads(Model m) {
		m.addAttribute("loginModel", new LoginEntityClass());
		return new ModelAndView("leads");
	}
	
	@RequestMapping(value = "/newleads", method = RequestMethod.GET)
	public ModelAndView newleads(Model m) {
		m.addAttribute("loginModel", new LoginEntityClass());
		return new ModelAndView("new_leads");
	}
	
	@RequestMapping(value = "/editleads", method = RequestMethod.GET)
	public ModelAndView editleads(Model m) {
		m.addAttribute("loginModel", new LoginEntityClass());
		return new ModelAndView("editleads");
	}
	@RequestMapping(value = "/customer", method = RequestMethod.GET)
	public ModelAndView customer(Model m) {
		m.addAttribute("loginModel", new LoginEntityClass());
		return new ModelAndView("customer");
	}
	@RequestMapping(value = "/editcustomer", method = RequestMethod.GET)
	public ModelAndView editcustomer(Model m) {
		m.addAttribute("loginModel", new LoginEntityClass());
		return new ModelAndView("editcustomer");
	}
	@RequestMapping(value = "/ticketlist", method = RequestMethod.GET)
	public ModelAndView ticketlist(Model m) {
		m.addAttribute("loginModel", new LoginEntityClass());
		return new ModelAndView("ticketlist");
	}
	@RequestMapping(value = "/newticket", method = RequestMethod.GET)
	public ModelAndView newticket(Model m) {
		m.addAttribute("loginModel", new LoginEntityClass());
		return new ModelAndView("newticket");
	}




}
