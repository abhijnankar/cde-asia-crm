package com.cde.crm.controller;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cde.crm.entity.EmployeeList;
import com.cde.crm.entity.IndentList;
import com.cde.crm.entity.ItemWorkEntity;
import com.cde.crm.entity.LeadsList;
import com.cde.crm.entity.VendorPricingItemList;
import com.cde.crm.repository.impl.VendorPricingListClass;

@RestController
@RequestMapping("/imsapitest")
public class ImsRestController {

	private final String sharedKey = "SHARED_KEY";
	private static final String SUCCESS_STATUS = "success";
	private static final String ERROR_STATUS = "error";
	private static final int CODE_SUCCESS = 100;
	private static final int AUTH_FAILURE = 102;

	@Autowired
	VendorPricingListClass vendorPricingListClass;

	@SuppressWarnings("unchecked")

	@RequestMapping(value = "/imstestapi", method = RequestMethod.GET)
	public ArrayList<VendorPricingItemList> homeview(@RequestParam(value = "key") String key, Model m) {
		ArrayList<VendorPricingItemList> code = new ArrayList<VendorPricingItemList>();
		VendorPricingItemList itemprice = new VendorPricingItemList();
		System.out.println(key);
		List<String> items = Arrays.asList(key.split("\\s*,\\s*"));
		StringJoiner joiner = new StringJoiner(",");
		for (CharSequence cs : items) {
			joiner.add("'" + cs + "'");
		}
		String str = joiner.toString();

		if (key != null && !key.equals("")) {
			List<VendorPricingItemList> editlist = vendorPricingListClass.itemlistapi(str);

			System.out.println(str);
			for (VendorPricingItemList item : editlist) {
				itemprice.setItemCode(item.getItemCode());
				itemprice.setItemDescription(item.getItemDescription());
				itemprice.setItemType(item.getItemType());
				itemprice.setUnitPrice(item.getUnitPrice());
				itemprice.setUom(item.getUom());
				itemprice.setWeight(item.getWeight());
				code.add(itemprice);
			}
			return (ArrayList<VendorPricingItemList>) editlist;

		} else {
			List<VendorPricingItemList> editlist = vendorPricingListClass.itemlistapiall();

			for (VendorPricingItemList item : editlist) {

				itemprice.setItemCode(item.getItemCode());
				itemprice.setItemDescription(item.getItemDescription());
				itemprice.setItemType(item.getItemType());
				itemprice.setUnitPrice(item.getUnitPrice());
				itemprice.setUom(item.getUom());
				itemprice.setWeight(item.getWeight());
				code.add(itemprice);
			}
			return (ArrayList<VendorPricingItemList>) editlist;
		}
		// return code;
		// return null;
		/*
		 * if (sharedKey.equalsIgnoreCase(key)) { int userId = request.getUserId();
		 * String itemId = request.getItemId(); double discount = request.getDiscount();
		 * // Process the request // .... // Return success response to the client.
		 * response.setStatus(SUCCESS_STATUS); response.setCode(CODE_SUCCESS); } else {
		 * response.setStatus(ERROR_STATUS); response.setCode(AUTH_FAILURE); }
		 */

	}

	@RequestMapping(value = "/indentapi", method = RequestMethod.GET)
	public ArrayList<IndentList> indentlist(@RequestParam(value = "indentid") String indentid, Model m) {
		IndentList itemprice = new IndentList();
		List<String> indents = Arrays.asList(indentid.split("\\s*,\\s*"));
		StringJoiner joiner = new StringJoiner(",");
		for (CharSequence cs : indents) {
			joiner.add("'" + cs + "'");
		}
		String str = joiner.toString();

		if (indentid != null && !indentid.equals("")) {
			List<IndentList> editlist = vendorPricingListClass.indentlistapi(str);
			System.out.println(str);
			return (ArrayList<IndentList>) editlist;

		} else {
			List<IndentList> editlist = vendorPricingListClass.indentlistapiall();
			return (ArrayList<IndentList>) editlist;
		}
	}

	@RequestMapping(value = "/newindentapi", method = RequestMethod.POST)
	public String newindentadd(InputStream incomingData, IndentList indentlist, HttpServletRequest req,
			HttpServletResponse res, Model m) throws JSONException {
		String str = "New Indent Creation";
		// String requestUrl = req.getRequestURI();
		StringBuilder crunchifyBuilder = new StringBuilder();
		JSONObject jsonObject = null;
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
			String line = null;
			while ((line = in.readLine()) != null) {
				crunchifyBuilder.append(line);
			}
			jsonObject = new JSONObject(crunchifyBuilder.toString());
			System.out.println(jsonObject);
		} catch (JSONException e) {
			System.out.println("Error Parsing: - ");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		indentlist.setIndentid(jsonObject.getString("indentid"));
		indentlist.setProjectid(jsonObject.getString("projectid"));
		indentlist.setRaisedby(jsonObject.getString("raisedby"));
		indentlist.setRaisedon(jsonObject.getString("raisedon"));
		System.out.println(jsonObject.getString("projectid"));
		List<String> list = new ArrayList<String>();
		JSONArray array = jsonObject.getJSONArray("itemDetails");
		for (int i = 0; i < array.length(); i++) {
			list.add(array.getJSONObject(i).getString("kitid"));
			indentlist.setKitid(array.getJSONObject(i).getString("kitid"));
			indentlist.setKittitle(array.getJSONObject(i).getString("kittitle"));
			indentlist.setKitdescription(array.getJSONObject(i).getString("kitdescription"));
			indentlist.setBomtype(array.getJSONObject(i).getString("bomtype"));
			indentlist.setItemcode(array.getJSONObject(i).getString("itemcode"));
			indentlist.setItemdesc(array.getJSONObject(i).getString("itemdesc"));
			indentlist.setIndqty(array.getJSONObject(i).getString("indqty"));
			List<IndentList> checkitem = vendorPricingListClass.checkitemByIndent(indentlist);
			if (checkitem.isEmpty()) {
				vendorPricingListClass.createnewindent(indentlist);
				str = "Inserted Successfully";
			} else {
				vendorPricingListClass.updatenewindent(indentlist);
				str = "Updated Successfully";
			}

		}

		return str;

	}

	@RequestMapping(value = "/matureleads", method = RequestMethod.GET)
	public ResponseEntity<String> matureleads(Model m) {
//		 @RequestParam(value = "indentid") String indentid
		LeadsList matureleads = new LeadsList();
		List<LeadsList> editlist = vendorPricingListClass.matureleads();
		editlist.size();
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		Gson userGson = new GsonBuilder().create();
		// JSONObject[] mainObj = new JSONObject();
		JSONArray mainObj = new JSONArray();
		JSONObject[] main = new JSONObject[editlist.size()];
		int j = 0;
		for (int i = 0; i < editlist.size(); i++) {
//			String JSONObject = gson.toJson(editlist.get(i));
//			Gson prettyGson = new GsonBuilder().setPrettyPrinting().create();
//			String prettyJson = prettyGson.toJson(editlist.get(i)).replaceAll("\\\\", "");
			JSONObject prettyJson = new JSONObject(editlist.get(i));
			//jsonObj = new JSONObject();
			JSONObject jsonObj = new JSONObject();
			try {
				jsonObj.put("projectName", editlist.get(i).getShortrequirement());
				jsonObj.put("projectCode", "");
				jsonObj.put("plantType", editlist.get(i).getPlanttype());
				jsonObj.put("projectDtls", editlist.get(i).getFullrequirement());
				jsonObj.put("projectSpecification", "");
				jsonObj.put("additionalDtls", "");
				jsonObj.put("specialFeaturesOfPlant", "");
				jsonObj.put("specialFeaturesOfPlant", "");
				jsonObj.put("purposeOfPlant", "");
				jsonObj.put("startDate", "2020-04-04");
				jsonObj.put("clientDetails", prettyJson);
				jsonObj.put("contactPersonDetails", "");
				jsonObj.put("contactEmergencyDetails", "");
				
				mainObj.put(jsonObj);
				// mainObj.put(jsonObj);
				System.out.println(jsonObj);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			j++;
		}
		System.out.println(mainObj);
		//return main;
		return new ResponseEntity<String>(mainObj.toString(), HttpStatus.OK);
		//return new ResponseEntity<String>(mainObj.toString().replaceAll("\\\\", ""), HttpStatus.OK);
	}

	@RequestMapping(value = "/projectcreation", method = RequestMethod.POST)
	public String projectcreation(InputStream incomingData, LeadsList leadlist, HttpServletRequest req,
			HttpServletResponse res, Model m) throws JSONException {
		String str = "New Project Creation";
		// String requestUrl = req.getRequestURI();
		StringBuilder crunchifyBuilder = new StringBuilder();
		JSONObject jsonObject = null;
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
			String line = null;
			while ((line = in.readLine()) != null) {
				crunchifyBuilder.append(line);
			}
			jsonObject = new JSONObject(crunchifyBuilder.toString());
			System.out.println(jsonObject);
		} catch (JSONException e) {
			System.out.println("Error Parsing: - ");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		leadlist.setProjectcode(jsonObject.getString("projectCode"));
		leadlist.setProjectname(jsonObject.getString("projectName"));
		leadlist.setProjectspecification(jsonObject.getString("projectSpecification"));
		leadlist.setPlanttype(jsonObject.getString("plantType"));
		leadlist.setProjectdetails(jsonObject.getString("projectDtls"));
		leadlist.setAdditionaldetails(jsonObject.getString("additionalDtls"));
		leadlist.setSpecialfeaturesofplant(jsonObject.getString("specialFeaturesOfPlant"));
		leadlist.setPurposeofplant(jsonObject.getString("purposeOfPlant"));
		leadlist.setStartdate(jsonObject.getString("startDate"));
		//jsonObject.getJSONObject("clientDetails").getString("customerid");
		System.out.println(jsonObject.getJSONObject("clientDetails").getString("customerid"));
		leadlist.setCustomerid(jsonObject.getJSONObject("clientDetails").getString("customerid"));
		leadlist.setLeadid(jsonObject.getJSONObject("clientDetails").getString("leadid"));
		vendorPricingListClass.createnewproject(leadlist);
		str = "Inserted Successfully";
		//List<String> list = new ArrayList<String>();
		/*
		 * JSONArray array = jsonObject.getJSONArray("clientDetails"); for(int i = 0 ; i
		 * < array.length() ; i++){
		 * leadlist.setCustomerid(array.getJSONObject(i).getString("customerid"));
		 * leadlist.setLeadid(array.getJSONObject(i).getString("leadid"));
		 * leadlist.setProjectcode(jsonObject.getString("projectCode"));
		 * leadlist.setProjectname(jsonObject.getString("projectName"));
		 * leadlist.setProjectspecification(jsonObject.getString("projectSpecification")
		 * ); leadlist.setPlanttype(jsonObject.getString("plantType"));
		 * leadlist.setProjectdetails(jsonObject.getString("projectDtls"));
		 * leadlist.setAdditionaldetails(jsonObject.getString("additionalDtls"));
		 * leadlist.setSpecialfeaturesofplant(jsonObject.getString(
		 * "specialFeaturesOfPlant"));
		 * leadlist.setPurposeofplant(jsonObject.getString("purposeOfPlant"));
		 * leadlist.setStartdate(jsonObject.getString("startDate"));
		 * vendorPricingListClass.createnewproject(leadlist); str
		 * ="Inserted Successfully";
		 * 
		 * List<IndentList> checkitem =
		 * vendorPricingListClass.checkitemByIndent(indentlist); if(checkitem.isEmpty())
		 * { vendorPricingListClass.createnewindent(indentlist); str =
		 * "Inserted Successfully"; } else {
		 * vendorPricingListClass.updatenewindent(indentlist); str =
		 * "Updated Successfully"; }
		 * 
		 * 
		 * }
		 */

		return str;

	}
	
	@RequestMapping(value = "/leadlist", method = RequestMethod.GET)
	public ArrayList<LeadsList> leadlist(Model m) {
		LeadsList matureleads = new LeadsList();
		List<LeadsList> editlist = vendorPricingListClass.matureleads();
		return (ArrayList<LeadsList>) editlist;

	}
	

	@RequestMapping(value = "/employeelist", method = RequestMethod.GET)
	public ArrayList<EmployeeList> employeelist(Model m) {
		EmployeeList employeelist = new EmployeeList();
		List<EmployeeList> editlist = vendorPricingListClass.employeelist();
		return (ArrayList<EmployeeList>) editlist;

	}
	
	@RequestMapping(value = "/itemworkmaster", method = RequestMethod.GET)
	public ArrayList<ItemWorkEntity> itemworkmaster(Model m) {
		ItemWorkEntity itemwork = new ItemWorkEntity();
		List<ItemWorkEntity> itemworkmaster = vendorPricingListClass.itemworkmaster();
		return (ArrayList<ItemWorkEntity>) itemworkmaster;

	}

	/*
	 * public String newindentadd(HttpServletRequest req, HttpServletResponse
	 * res,@RequestParam(value = "projectid") String projectid ,@RequestParam(value
	 * = "indentid") String indentid ,@RequestParam(value = "kitid[]") String[]
	 * kitid ,@RequestParam(value = "kittitle[]") String[] kittitle
	 * ,@RequestParam(value = "kitdescription[]") String[] kitdescription
	 * ,@RequestParam(value = "bomtype[]") String[] bomtype ,@RequestParam(value =
	 * "itemcode[]") String[] itemcode ,@RequestParam(value = "itemdesc[]") String[]
	 * itemdesc ,@RequestParam(value = "indqty[]") String[] indqty
	 * ,@RequestParam(value = "raisedby") String raisedby ,@RequestParam(value =
	 * "raisedon") String raisedon ,Model m) {
	 * 
	 * System.out.println(indentid); System.out.println(projectid);
	 * System.out.println(req);
	 * 
	 * 
	 * int i = 0; for(String items : itemcode) { String qty = indqty[i]; String
	 * itdesc = itemdesc[i]; String kit = kitid[i]; String title = kittitle[i];
	 * String desc = kitdescription[i]; String boms = bomtype[i]; List<IndentList>
	 * checkitem = vendorPricingListClass.checkitem(items,indentid,projectid);
	 * if(checkitem.isEmpty()) {
	 * vendorPricingListClass.createindent(indentid,projectid,kit,title,desc,boms,
	 * items,itdesc,qty,raisedby,raisedon); } else {
	 * vendorPricingListClass.updateindent(indentid,projectid,kit,title,desc,boms,
	 * items,itdesc,qty,raisedby,raisedon); }
	 * 
	 * i++; }
	 * 
	 * String str = "Inserted Successfully"; return str;
	 * 
	 * }
	 */

}
