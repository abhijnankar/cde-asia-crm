<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

 <jsp:include page="/crmheader.jsp"></jsp:include>
<jsp:include page="/crmsidebar.jsp"></jsp:include>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                        <!-- Page/module tite -->
                        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                            <h1 class="h2">All tickets</h1>
                            <div class="btn-toolbar mb-2 mb-md-0">
                                <a class="btn btn-sm btn-primary px-3" href="newticket">
                                    <span data-feather="plus"></span>
                                    Create new ticket
                                </a>
                            </div>
                        </div>

                        <!-- Quick Statistic -->
                        <div  class="section-wrap mb-4">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="widget">
                                        <span class="caption">Opened ticket</span>
                                        <span class="value">330</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="widget">
                                        <span class="caption">Work in progress</span>
                                        <span class="value">68</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                     <div class="widget">
                                         <span class="caption">Total ticket</span>
                                         <span class="value">3,93,116</span>
                                     </div>
                                 </div>
                            </div>
                         </div><!-- quick stats -->
                         <hr>

                         <!-- first card -->
                         <div class="card">
                             
                             <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <h3 class="title">All ticket request</h3>
                                            <table class="table table-bordered table-sm" id="mainInventoryTable2">
                                                <thead>
                                                  <tr>
                                                    <th>Ticket No.</th>
                                                    <th>Customer Name</th>
                                                    <th>Service type</th>
                                                    <th>Severity</th>
                                                    <th>Asignee</th>
                                                    <th>Actions</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td><strong>TKT020/001461</strong></td>
                                                    <td>Prama Manufacturing Pvt. Ltd.</td>
                                                    <td>Material</td>
                                                    <td><span class="badge-pill badge-warning">Medium</span></td>
                                                    <td>
                                                        <div class="assignee">
                                                            <img src="https://randomuser.me/api/portraits/men/10.jpg" data-toggle="tooltip" data-placement="top" title="John Doe">
                                                            <img src="https://randomuser.me/api/portraits/women/10.jpg" data-toggle="tooltip" data-placement="top" title="Jane Doe">
                                                            <img src="https://randomuser.me/api/portraits/men/80.jpg" data-toggle="tooltip" data-placement="top" title="Lorem Doe">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(0);" class="badge badge-pill badge-primary font-fix" data-toggle="modal" data-target="#allocateModal">Assign to</a>
                                                        <a href="view-ticket-details.html" class="badge badge-pill badge-secondary font-fix">View details</a>
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <td><strong>TKT020/001461</strong></td>
                                                    <td>Prama Manufacturing Pvt. Ltd.</td>
                                                    <td>Material</td>
                                                    <td><span class="badge-pill badge-success">Normal</span></td>
                                                    <td>
                                                        <div class="assignee">
                                                            <img src="https://randomuser.me/api/portraits/men/7.jpg" data-toggle="tooltip" data-placement="top" title="John Doe">
                                                            <img src="https://randomuser.me/api/portraits/women/5.jpg" data-toggle="tooltip" data-placement="top" title="Jane Doe">
                                                            <img src="https://randomuser.me/api/portraits/men/9.jpg" data-toggle="tooltip" data-placement="top" title="Lorem Doe">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(0);" class="badge badge-pill badge-primary font-fix" data-toggle="modal" data-target="#allocateModal">Assign to</a>
                                                        <a href="view-ticket-details.html" class="badge badge-pill badge-secondary font-fix">View details</a>
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <td><strong>TKT020/001461</strong></td>
                                                    <td>Prama Manufacturing Pvt. Ltd.</td>
                                                    <td>Material</td>
                                                    <td><span class="badge-pill badge-danger">Critical</span></td>
                                                    <td>
                                                        <div class="assignee">
                                                            <img src="https://randomuser.me/api/portraits/men/30.jpg" data-toggle="tooltip" data-placement="top" title="John Doe">
                                                            <img src="https://randomuser.me/api/portraits/women/30.jpg" data-toggle="tooltip" data-placement="top" title="Jane Doe">
                                                            <img src="https://randomuser.me/api/portraits/men/21.jpg" data-toggle="tooltip" data-placement="top" title="Lorem Doe">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(0);" class="badge badge-pill badge-primary font-fix" data-toggle="modal" data-target="#allocateModal">Assign to</a>
                                                        <a href="view-ticket-details.html" class="badge badge-pill badge-secondary font-fix">View details</a>
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <td><strong>TKT020/001461</strong></td>
                                                    <td>Prama Manufacturing Pvt. Ltd.</td>
                                                    <td>Material</td>
                                                    <td><span class="badge-pill badge-warning">Medium</span></td>
                                                    <td>
                                                        <div class="assignee">
                                                            <img src="https://randomuser.me/api/portraits/men/11.jpg" data-toggle="tooltip" data-placement="top" title="John Doe">
                                                            <img src="https://randomuser.me/api/portraits/women/11.jpg" data-toggle="tooltip" data-placement="top" title="Jane Doe">
                                                            <img src="https://randomuser.me/api/portraits/men/11.jpg" data-toggle="tooltip" data-placement="top" title="Lorem Doe">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(0);" class="badge badge-pill badge-primary font-fix" data-toggle="modal" data-target="#allocateModal">Assign to</a>
                                                        <a href="view-ticket-details.html" class="badge badge-pill badge-secondary font-fix">View details</a>
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <td><strong>TKT020/001461</strong></td>
                                                    <td>Prama Manufacturing Pvt. Ltd.</td>
                                                    <td>Material</td>
                                                    <td><span class="badge-pill badge-success">Normal</span></td>
                                                    <td>
                                                        <div class="assignee">
                                                            <img src="https://randomuser.me/api/portraits/men/18.jpg" data-toggle="tooltip" data-placement="top" title="John Doe">
                                                            <img src="https://randomuser.me/api/portraits/women/11.jpg" data-toggle="tooltip" data-placement="top" title="Jane Doe">
                                                            <img src="https://randomuser.me/api/portraits/men/88.jpg" data-toggle="tooltip" data-placement="top" title="Lorem Doe">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(0);" class="badge badge-pill badge-primary font-fix" data-toggle="modal" data-target="#allocateModal">Assign to</a>
                                                        <a href="view-ticket-details.html" class="badge badge-pill badge-secondary font-fix">View details</a>
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <td><strong>TKT020/001461</strong></td>
                                                    <td>Prama Manufacturing Pvt. Ltd.</td>
                                                    <td>Material</td>
                                                    <td><span class="badge-pill badge-warning">Medium</span></td>
                                                    <td>
                                                        <div class="assignee">
                                                            <img src="https://randomuser.me/api/portraits/men/10.jpg" data-toggle="tooltip" data-placement="top" title="John Doe">
                                                            <img src="https://randomuser.me/api/portraits/women/10.jpg" data-toggle="tooltip" data-placement="top" title="Jane Doe">
                                                            <img src="https://randomuser.me/api/portraits/men/80.jpg" data-toggle="tooltip" data-placement="top" title="Lorem Doe">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(0);" class="badge badge-pill badge-primary font-fix" data-toggle="modal" data-target="#allocateModal">Assign to</a>
                                                        <a href="view-ticket-details.html" class="badge badge-pill badge-secondary font-fix">View details</a>
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <td><strong>TKT020/001461</strong></td>
                                                    <td>Prama Manufacturing Pvt. Ltd.</td>
                                                    <td>Material</td>
                                                    <td><span class="badge-pill badge-warning">Medium</span></td>
                                                    <td>
                                                        <div class="assignee">
                                                            <img src="https://randomuser.me/api/portraits/men/10.jpg" data-toggle="tooltip" data-placement="top" title="John Doe">
                                                            <img src="https://randomuser.me/api/portraits/women/10.jpg" data-toggle="tooltip" data-placement="top" title="Jane Doe">
                                                            <img src="https://randomuser.me/api/portraits/men/80.jpg" data-toggle="tooltip" data-placement="top" title="Lorem Doe">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(0);" class="badge badge-pill badge-primary font-fix" data-toggle="modal" data-target="#allocateModal">Assign to</a>
                                                        <a href="view-ticket-details.html" class="badge badge-pill badge-secondary font-fix">View details</a>
                                                    </td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                        </div>

                                    </div>
                                </div>

                             </div>
                         </div>
                         <!-- /first card -->

                         <!-- Assign on Ticket -->
                         <div class="modal fade" id="allocateModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="allocateModalTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalCenterTitle">Assign to ticket</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="">Assign to</label>
                                        <select class="form-control">
                                            <option>John doe</option>
                                            <option>Jane doe</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Assign</button>
                                </div>
                            </div>
                            </div>
                        </div>

                    </main>
                </div>
            </div>
        </section>
        <jsp:include page="/crmfooter.jsp"></jsp:include>