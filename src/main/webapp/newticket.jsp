<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

 <jsp:include page="/crmheader.jsp"></jsp:include>
<jsp:include page="/crmsidebar.jsp"></jsp:include>

 <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                        <!-- Page/module tite -->
                        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                            <h1 class="h2">Create a new ticket</h1>
                            <!-- <div class="btn-toolbar mb-2 mb-md-0">
                                <a class="btn btn-sm btn-primary px-3" href="new-ticket.html">
                                    <span data-feather="plus"></span>
                                    Create new ticket
                                </a>
                            </div> -->
                        </div>

                         <!-- first card -->
                         <div class="card">
                             
                             <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="">
                                            <h3 class="title">Basic details</h3>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="">Customer name</label>
                                                        <input class="form-control" placeholder="Customer name">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="">Select service</label>
                                                        <select class="form-control">
                                                            <option>Plant for something</option>
                                                            <option>10mm Sand machine</option>
                                                            <option>Lorem ipsum</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="">Severity</label>
                                                        <select class="form-control">
                                                            <option>Normal</option>
                                                            <option>Medium</option>
                                                            <option>Critical</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="">Assignee</label>
                                                        <select class="form-control">
                                                            <option>John Doe</option>
                                                            <option>Jane Doe</option>
                                                            <option>Some name</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>

                                            <h3 class="title">Issue details</h3>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="">Define issue</label>
                                                        <textarea class="form-control" rows="1"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="">Upload document</label>
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input" id="customFile">
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mt-4">
                                                <div class="col-md-12">
                                                    <button class="btn btn-danger">Clear</button>
                                                    <button class="btn btn-primary">Create</button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                             </div>
                         </div>
                         <!-- /first card -->

                         <!-- Assign on Ticket -->
                         <div class="modal fade" id="allocateModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="allocateModalTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalCenterTitle">Assign to ticket</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="">Assign to</label>
                                        <select class="form-control">
                                            <option>John doe</option>
                                            <option>Jane doe</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Assign</button>
                                </div>
                            </div>
                            </div>
                        </div>

                    </main>
                </div>
            </div>
        </section>
        
         <jsp:include page="/crmfooter.jsp"></jsp:include>
         