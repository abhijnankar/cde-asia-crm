<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

 <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- <link rel="icon" href="#"> -->

        <title>CRM Dashboard | CDE Asia Ltd.</title>

        <!-- Bootstrap core CSS -->
        <link href="<c:url value="/resources/assets/core/bootstrap.css" />" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="<c:url value="/resources/assets/style/dashboard.css" />" rel="stylesheet">
        <!-- datepicker -->
        <link href="<c:url value="/resources/assets/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/assets/plugins/select2/css/select2.css" />" rel="stylesheet">
        <!-- multiselect -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">
        <!-- data table -->
        <link href="https://auto-clinic.in/cde_ims/assets/vendors/DataTables/datatables.min.css" rel="stylesheet" />
    </head>

    <body>
        <!-- Header navbar -->
        <header class="sticky-top">
            <nav class="navbar navbar-dark bg-brand flex-md-nowrap p-0">
                <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="index.html">
                    <img src="<c:url value="/resources/assets/images/brand/logo.png" />" alt="">
                </a>
                <ul class="navbar-nav px-3 notification-bell">
                    <li class="nav-item text-wrap bell dropdown">
                        <a class="nav-link has-notification" href="#" data-toggle="dropdown"><span class="modd" data-feather="bell"></span></a>
                        <div class="dropdown-menu">
                            <h4 class="dropdown-header">Notifications</h4>
                            <a class="dropdown-item" href="#">Requested for purchase order</a>
                            <a class="dropdown-item" href="#">Lorem ipsum sit amet dollor is dummy text</a>
                            <a class="dropdown-item" href="#">Lorem ipsum sit amet dollor is dummy text</a>
                        </div>
                    </li>
                    <li class="nav-item text-nowrap">
                        <a class="nav-link" href="#">Sign out</a>
                    </li>
                </ul>
            </nav>
        </header>