<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<jsp:include page="/vendorheader.jsp"></jsp:include>
<jsp:include page="/vendorsidebar.jsp"></jsp:include>
<%-- welcome <%=session.getAttribute("uname") %> --%>

<div class="content-wrapper">
	<!-- START PAGE CONTENT-->
	<div class="page-content fade-in-up">
		<div class="row">

			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-head" style="background-color: #512DA8;">
						<div class="ibox-title" style="color: white;">Vendor Pricing List</div>
						<!-- <div class="ibox-title"><input type="text" id="indentSearchDiv" style="height: 25px; font-size: 11px;" placeholder="Search ....." size="50px"/></div> -->
					</div>
                   
					<div class="ibox-body">
						<table class="table table-striped table-bordered table-hover"
							id="pricingDataTable" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Item Code</th>
									<th>Item Description</th>
									<th>Item Type</th>
									<th>Unit Price</th>
									<th>UOM</th>
									<th>Weight</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${pricinglist}" var="pricinglist">
									<tr>
										<td>${pricinglist.itemCode}</td>
										<td>${pricinglist.itemDescription}</td>
										<td>${pricinglist.itemType}</td>
										<td>${pricinglist.unitPrice}</td>
										<td>${pricinglist.uom}</td>
										<td>${pricinglist.weight}</td>
                                        <td>
                                         <form:form id="editTableform" method="post" action="/CDEVendor/editPrice">
                                       <button	onclick="editList('${pricinglist.itemCode}')" class="btn btn-info btn-sm">EditPrice</button>
												<input type="hidden"  name="itemCode"
													value="${pricinglist.itemCode}" />
                                          </form:form>
                                        
                                        
                                        </td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
                 
				</div>
			</div>
			<!-- polist -->
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-head" style="background-color: #512DA8;">
						<div class="ibox-title" style="color: white;">PO Acceptance List</div>
						<!-- <div class="ibox-title"><input type="text" id="indentSearchDiv" style="height: 25px; font-size: 11px;" placeholder="Search ....." size="50px"/></div> -->
					</div>
                   
					<div class="ibox-body">
						<table class="table table-striped table-bordered table-hover"
							id="poListDataTable" cellspacing="0" width="100%">
							<thead>
								<tr>
								
									<th>Indent ID</th>
									<th>Project ID</th>
									<th>PO Number</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${vpolist}" var="vpolist">
									<tr>
										<td>${vpolist.indentID}</td>
										<td>${vpolist.projectID}</td>
										<td>${vpolist.poNumber}</td>
                                        <td>
                                         <form:form id="poTableform" method="post" action="/CDEVendor/viewPODetails" modelAttribute="viewPODetails">
                                       <button	onclick="poList('${vpolist.indentID}','${vpolist.projectID}','${vpolist.poNumber}')" class="btn btn-info btn-sm">PODetails</button>
												<form:input type="hidden" path="indentID"  name="indentID" value="${vpolist.indentID}" />
													<form:input type="hidden" path="projectID"  name="projectID" value="${vpolist.projectID}" />
														<form:input type="hidden" path="poNumber"  name="poNumber" value="${vpolist.poNumber}" />
                                          </form:form>
                                         
                                        
                                        </td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
                 
				</div>
			</div>

		</div>
	</div>
	<!-- END PAGE CONTENT-->

<jsp:include page="/vendorfooter.jsp"></jsp:include>
<script>
	$(document).ready(function() {
		var purchaseTable = $('#pricingDataTable').DataTable({
			
			pageLength : 5,
		});
	})
	
	$(document).ready(function() {
		var purchaseTable = $('#poListDataTable').DataTable({
			
			pageLength : 5,
		});
	})
	
	function editList(itemCode){	
		alert(itemCode);
		document.getElementById("editTableform").submit();
	}
	
	
	
	function poList(indentID,projectID,poNumber){	
		alert(poNumber);
		document.getElementById("poTableform").submit();
	}
	</script>