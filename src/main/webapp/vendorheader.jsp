<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<head>
      <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>Vendor Dashboard</title>
    <!-- GLOBAL MAINLY STYLES-->
     <link rel="stylesheet" href="<c:url value="/resources/assets2/vendors/bootstrap/dist/css/bootstrap.min.css" />">
     <link rel="stylesheet" href="<c:url value="/resources/assets2/vendors/font-awesome/css/font-awesome.min.css" />"> 
     <link rel="stylesheet" href="<c:url value="/resources/assets2/vendors/themify-icons/css/themify-icons.css" />"> 
    <!-- PLUGINS STYLES-->
     <link rel="stylesheet" href="<c:url value="/resources/assets2/vendors/DataTables/datatables.min.css" />">
     <link rel="stylesheet" href="<c:url value="/resources/assets2/vendors/morris.js/morris.css" />">

    <!-- THEME STYLES-->
     <link rel="stylesheet" href="<c:url value="/resources/assets2/css/main.min.css" />">
    <style type="text/css">
        .relative {
            position: relative;
        }
        .absolute-center {
            position:absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
        .text-center{
            text-align: center;
        }
        .circle-chart-text {
            font-size:14px;
        }
        .stepwizard-step p {
            margin-top: 0px;
            color:#666;
        }
        .stepwizard-row {
            display: table-row;
        }
        .stepwizard {
            display: table;
            width: 100%;
            position: relative;
        }
        .stepwizard-step button[disabled] {
            /*opacity: 1 !important;
            filter: alpha(opacity=100) !important;*/
        }
        .stepwizard .btn.disabled, .stepwizard .btn[disabled], .stepwizard fieldset[disabled] .btn {
            opacity:1 !important;
            color:#bbb;
        }
        .stepwizard-row:before {
            top: 40px;
            bottom: 0;
            position: absolute;
            content:" ";
            width: 90%;
            height: 10px;
            background-color: #e3e6e7;
            z-index: 0;
            left:7%;
        }
        .stepwizard-step {
            display: table-cell;
            text-align: center;
            position: relative;
        }
        .btn-circle {
            width: 30px;
            height: 30px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
        }
        .btn-small {
            color:green;
            border-color: #e3e6e7;
        }
        .btn-large {
            width: 90px !important;
            height: 90px !important;
            border-radius: 45px !important;
            line-height: 80px !important;
            border-color: #e3e6e7;
        }
        .align-left {
            text-align: left !important;
        }
        .align-center {
            text-align: center !important;
        }
        .align-right {
            text-align: right !important;
        }
    </style>
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        <header class="header">
            <div class="page-brand">
                <a class="link" href="https://auto-clinic.in/cde_procurement/">
                    <span class="brand navbar-brand">
                    <img src='<c:url value="/resources/assets2/img/logos/logo.png" />' alt="CDEVendor">
                    </span>
                    <span class="brand-mini"><img src='<c:url value="/resources/assets2/img/logos/logo.png" />' alt="CDEVendor"></span>
                </a>
            </div>
            
            <div class="flexbox flex-1">
            	<!-- START TOP-LEFT TOOLBAR-->
                <ul class="nav navbar-toolbar">
                    <li>
                        <a class="nav-link sidebar-toggler js-sidebar-toggler"><i class="ti-menu"></i></a>
                    </li>
                </ul>
                <!-- END TOP-LEFT TOOLBAR-->
                <ul class="nav navbar-toolbar">
                    <li class="dropdown dropdown-notification">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell-o rel"><span class="notify-signal"></span></i></a>
                        <ul class="dropdown-menu dropdown-menu-right dropdown-menu-media">
                            <li class="dropdown-menu-header">
                                <div>
                                    <span><strong>5 New</strong> Notifications</span>
                                    <a class="pull-right" href="javascript:;">view all</a>
                                </div>
                            </li>
                            <li class="list-group list-group-divider scroller" data-height="240px" data-color="#71808f">
                                <div>
                                    <a class="list-group-item">
                                        <div class="media">
                                            <div class="media-img">
                                                <span class="badge badge-success badge-big"><i class="fa fa-check"></i></span>
                                            </div>
                                            <div class="media-body">
                                                <div class="font-13">4 task compiled</div><small class="text-muted">22 mins</small></div>
                                        </div>
                                    </a>
                                    <a class="list-group-item">
                                        <div class="media">
                                            <div class="media-img">
                                                <span class="badge badge-default badge-big"><i class="fa fa-shopping-basket"></i></span>
                                            </div>
                                            <div class="media-body">
                                                <div class="font-13">You have 12 new orders</div><small class="text-muted">40 mins</small></div>
                                        </div>
                                    </a>
                                    <a class="list-group-item">
                                        <div class="media">
                                            <div class="media-img">
                                                <span class="badge badge-danger badge-big"><i class="fa fa-bolt"></i></span>
                                            </div>
                                            <div class="media-body">
                                                <div class="font-13">Server #7 rebooted</div><small class="text-muted">2 hrs</small></div>
                                        </div>
                                    </a>
                                    <a class="list-group-item">
                                        <div class="media">
                                            <div class="media-img">
                                                <span class="badge badge-success badge-big"><i class="fa fa-user"></i></span>
                                            </div>
                                            <div class="media-body">
                                                <div class="font-13">New user registered</div><small class="text-muted">2 hrs</small></div>
                                        </div>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-user">
                        <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
                            <img src="https://auto-clinic.in/cde_procurement/assets/img/admin-avatar.png" />
                            <span></span>Admin<i class="fa fa-angle-down m-l-5"></i></a>
							<ul class="dropdown-menu dropdown-menu-right">
								
								<a class="dropdown-item" href="/CDEVendor/"><i class="fa fa-power-off"></i>Logout</a>
							</ul>
                    </li>
                </ul>
                <!-- END TOP-RIGHT TOOLBAR-->
            </div>
        </header>