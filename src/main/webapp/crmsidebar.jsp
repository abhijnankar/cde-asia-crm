<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!-- START SIDEBAR-->
        <section>
            <div class="container-fluid">
                <div class="row">
                    <nav class="col-md-2 d-none d-md-block sidebar">
                        <div class="sidebar-sticky">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link active" href="index.html">
                                        <span data-feather="home"></span>
                                        Dashboard <span class="sr-only">(current)</span>
                                    </a>
                                </li>

                                <!-- Dropdown menu -->
                                <li data-toggle="collapse" data-target="#Marketing" class="collapsed nav-item">
                                    <a href="javascript:void(0);" class="nav-link"><span data-feather="minimize-2"></span> Marketing <span data-feather="chevron-down" class="has-dd"></span></a>
                                </li>
                                <ul class="sub-menu collapse" id="Marketing">
                                    <li><a href="leads" class="nav-link">Leads</a></li>
                                    <li><a href="customer" class="nav-link">Customer</a></li>
                                </ul><!-- /Dropdown menu -->

                               

                              
                                <!-- Dropdown menu -->
                                <li data-toggle="collapse" data-target="#Support" class="collapsed nav-item">
                                    <a href="javascript:void(0);" class="nav-link"><span data-feather="life-buoy"></span> Custom care <span data-feather="chevron-down" class="has-dd"></span></a>
                                </li>
                                <ul class="sub-menu collapse" id="Support">
                                    <li><a href="ticketlist" class="nav-link">All ticket</a></li>
                                    <li><a href="newticket" class="nav-link">New ticket</a></li>
                                </ul><!-- /Dropdown menu -->

                            </ul>
                           
                        </div>
                    </nav>
        <!-- END SIDEBAR-->