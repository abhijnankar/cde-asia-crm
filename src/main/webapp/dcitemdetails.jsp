<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<jsp:include page="/vendorheader.jsp"></jsp:include>
<jsp:include page="/vendorsidebar.jsp"></jsp:include>
<style>
.page-footer{
	bottom: auto;
}
</style>
<%-- welcome <%=session.getAttribute("uname") %> --%>
<div class="content-wrapper">
   <!-- START PAGE CONTENT-->
   <div class="page-content fade-in-up">
      <div class="row">
         <div class="col-lg-12">
            <div class="ibox">
               <div class="ibox-head">
                  <div class="ibox-title">details</div>
               </div>
               <div class="ibox-body">
                  <div class="row">
                     <%-- <div class="col-sm-3 form-group">
                        <label>Project Number</label> <input class="form-control"
                           type="text" placeholder="Project ID" readonly
                           value="${vendorPOListEntity.projectID}">
                     </div>
                     <div class="col-sm-3 form-group">
                        <label>Indent Number</label> <input class="form-control" type="text"
                           placeholder="Indent ID" readonly
                           value="${vendorPOListEntity.indentID}">
                     </div> --%>
                     <div class="col-sm-3 form-group">
                        <label>DC Number</label> <input class="form-control" type="text"
                           placeholder="DC Number" readonly
                           value="${deliverychallan}">
                     </div>
                  </div>
               </div>
            </div>
            <div class="ibox">
               <div class="ibox-head" style="background-color: #0077c0;">
                  <div class="ibox-title" style="color: white;">DC Item Details</div>
                  <!-- <div class="ibox-title"><input type="text" id="stdItemSearchDiv" style="height: 25px; font-size: 11px;" placeholder="Search ....." size="50px"/></div> -->
               </div>
               <div class="ibox-body">
                  <form:form method="post" action="/CDEVendor/dcreturnitems" enctype="multipart/form-data" modelAttribute="viewPODetails">
                  <input type="hidden" name="dcnumber" value="${deliverychallan}">
                     <table class="table table-striped table-bordered table-hover"
                        id="itemPoDetails" cellspacing="0" width="100%">
                        <thead>
                           <tr>
                              <!-- <th><input type="checkbox" id="checkall"></th> -->
                              <th>Item Code</th>
                              <th>Item Description</th>
                              <th>Item Type</th>
                              <th>Unit Price</th>
                              <th>Weight</th>
                              <th>UOM</th>
                              <th>DC Quantity</th>
                              <th>Return Quantity</th>
                              <th>Remaining Quantity</th>
                           </tr>
                        </thead>
                        <tbody>
                           <c:forEach items="${dcitemlist}" var="dcitemlist">
                              <tr>
                              <%--    <td><input type="checkbox" id="checkindent" path="itemCode" name="itemcodes[]" value="${dcitemlist.itemCode}"></td> --%>
                                 <td>${dcitemlist.itemCode}<input type="hidden" id="checkindent" path="itemCode" name="itemcodes[]" value="${dcitemlist.itemCode}"></td>
                                 <td>${dcitemlist.itemDescription}</td>
                                 <td>
                                    <c:if test="${dcitemlist.itemType == 'Y'}">										
                                       <% out.println("Fabrication(CDE-Asia)"); %>
                                    </c:if>
                                    <c:if test="${dcitemlist.itemType == 'X'}">											
                                       <% out.println("Purchase(CDE-Asia)"); %>
                                    </c:if>
                                    <c:if test="${dcitemlist.itemType == 'C'}">											
                                       <% out.println("Fabrication(Global)"); %>
                                    </c:if>
                                    <c:if test="${dcitemlist.itemType == 'P'}">											
                                       <% out.println("Purchase(Global)"); %>
                                    </c:if>
                                 </td>
                                 <td>${dcitemlist.unitPrice}</td>
                                 <td>${dcitemlist.weight}</td>
                                 <td>${dcitemlist.uom}</td>
                                 <td><input type="number" name="dcqnty[]" readonly="true" value="${dcitemlist.dcqty}"></td>
                                 <td>
                                 	<c:if test="${dcitemlist.dcqty == dcitemlist.returnqty }">
                                 		<input type="number" name="returnqnty[]" min="0" readonly="true" max="${dcitemlist.dcqty}" value="${dcitemlist.returnqty}">
                                 	</c:if>
                                 	<c:if test="${dcitemlist.dcqty != dcitemlist.returnqty }">
                                 		<input type="number" name="returnqnty[]" min="0" max="${dcitemlist.dcqty}" value="${dcitemlist.returnqty}">
                                 	</c:if>
                                 	
                                 	</td>
                                 <td><input type="number" name="remqnty[]" value="${dcitemlist.remqty}" readonly="true"></td>
                              </tr>
                           </c:forEach>
                        </tbody>
                     </table>
                     <button type="submit" class="btn btn-info btn-sm" style="margin: 20px;">Return</button>
                  </form:form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<!-- END PAGE CONTENT-->
<jsp:include page="/vendorfooter.jsp"></jsp:include>
<script>
   $(document).ready(function() {
   	var purchaseTable = $('#itemPoDetails').DataTable({
   
   		pageLength : 10,
   	});
   })
</script>
<script type="text/javascript">
   $('#checkall').click(function(e){
       var table= $(e.target).closest('table');
       $('td input:checkbox',table).prop('checked',this.checked);
   });
</script>