<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<jsp:include page="/vendorheader.jsp"></jsp:include>
<jsp:include page="/vendorsidebar.jsp"></jsp:include>
<%-- welcome <%=session.getAttribute("uname") %> --%>

<div class="content-wrapper">
	<!-- START PAGE CONTENT-->
	<div class="page-content fade-in-up">
		<div class="row">

			
			<!-- polist -->
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-head" style="background-color: #0077c0;">
						<div class="ibox-title" style="color: white;">PO Received List</div>
						<!-- <div class="ibox-title"><input type="text" id="indentSearchDiv" style="height: 25px; font-size: 11px;" placeholder="Search ....." size="50px"/></div> -->
					</div>
                   
					<div class="ibox-body">
						<table class="table table-striped table-bordered table-hover"
							id="poListDataTable" cellspacing="0" width="100%">
							<thead>
								<tr>
								
									<th>Indent Number</th>
									<th>Project Number</th>
									<th>PO Number</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${vpolist}" var="vpolist">
									<tr>
										<td>${vpolist.indentID}</td>
										<td>${vpolist.projectID}</td>
										<td>${vpolist.poNumber}</td>
                                        <td>
                                         <form:form id="poTableform" method="post" action="/CDEVendor/viewPODetails" modelAttribute="viewPODetails">
                                       <button	onclick="poList('${vpolist.indentID}','${vpolist.projectID}','${vpolist.poNumber}')" class="btn btn-info btn-sm">PODetails</button>
												<form:input type="hidden" path="indentID"  name="indentID" value="${vpolist.indentID}" />
													<form:input type="hidden" path="projectID"  name="projectID" value="${vpolist.projectID}" />
														<form:input type="hidden" path="poNumber"  name="poNumber" value="${vpolist.poNumber}" />
                                          </form:form>
                                         
                                        
                                        </td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
                 
				</div>
				
				
				<div class="ibox">
					<div class="ibox-head" style="background-color: #0077c0;">
						<div class="ibox-title" style="color: white;">PO Accept/Reject List</div>
						<!-- <div class="ibox-title"><input type="text" id="indentSearchDiv" style="height: 25px; font-size: 11px;" placeholder="Search ....." size="50px"/></div> -->
					</div>
                   
					<div class="ibox-body">
						<table class="table table-striped table-bordered table-hover"
							id="poStatListDataTable" cellspacing="0" width="100%">
							<thead>
								<tr>
								
									<th>Indent Number</th>
									<th>Project Number</th>
									<th>PO Number</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${postatlist}" var="postatlist">
									<tr>
										<td>${postatlist.indentID}</td>
										<td>${postatlist.projectID}</td>										
										<td>${postatlist.poNumber}</td>
										<td>${postatlist.status}</td>
                                        <td>
                                         <form:form id="poTableform" method="post" action="/CDEVendor/viewPOstatDetails" modelAttribute="viewPODetails">
                                       <button	onclick="poList('${postatlist.indentID}','${postatlist.projectID}','${postatlist.poNumber}')" class="btn btn-info btn-sm">Details</button>
												<form:input type="hidden" path="indentID"  name="indentID" value="${postatlist.indentID}" />
													<form:input type="hidden" path="projectID"  name="projectID" value="${postatlist.projectID}" />
														<form:input type="hidden" path="poNumber"  name="poNumber" value="${postatlist.poNumber}" />
                                          </form:form>
                                         
                                        
                                        </td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
                 
				</div>
			</div>

		</div>
	</div>
	<!-- END PAGE CONTENT-->

<jsp:include page="/vendorfooter.jsp"></jsp:include>
<script>
	$(document).ready(function() {
		var purchaseTable = $('#poStatListDataTable').DataTable({
			
			//pageLength : 10,
		});
	})
	
	$(document).ready(function() {
		var purchaseTable = $('#poListDataTable').DataTable({
			
			//pageLength : 1,
		});
	})
	
	function editList(itemCode){	
		alert(itemCode);
		document.getElementById("editTableform").submit();
	}
	
	
	
	function poList(indentID,projectID,poNumber){	
		alert(poNumber);
		document.getElementById("poTableform").submit();
	}
	</script>