<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<jsp:include page="/vendorheader.jsp"></jsp:include>
<jsp:include page="/vendorsidebar.jsp"></jsp:include>
<style>
.page-footer{
	bottom: auto;
}
</style>
<%-- welcome <%=session.getAttribute("uname") %> --%>
<div class="content-wrapper">
   <!-- START PAGE CONTENT-->
   <div class="page-content fade-in-up">
      <div class="row">
         <div class="col-lg-12">
            <div class="ibox">
               <div class="ibox-head">
                  <div class="ibox-title">details</div>
               </div>
               <div class="ibox-body">
                  <div class="row">
                     <div class="col-sm-3 form-group">
                        <label>Project Number</label> <input class="form-control"
                           type="text" placeholder="Project Number" readonly
                           value="${vendorPOListEntity.projectID}">
                     </div>
                     <div class="col-sm-3 form-group">
                        <label>Indent Number</label> <input class="form-control" type="text"
                           placeholder="Indent Number" readonly
                           value="${vendorPOListEntity.indentID}">
                     </div>
                     <div class="col-sm-3 form-group">
                        <label>PO Number</label> <input class="form-control" type="text"
                           placeholder="PO Number" readonly
                           value="${vendorPOListEntity.poNumber}">
                     </div>
                  </div>
               </div>
            </div>
            <div class="ibox">
               <div class="ibox-head" style="background-color: #0077c0;">
                  <div class="ibox-title" style="color: white;">Item PO Details</div>
                  <!-- <div class="ibox-title"><input type="text" id="stdItemSearchDiv" style="height: 25px; font-size: 11px;" placeholder="Search ....." size="50px"/></div> -->
               </div>
               <div class="ibox-body">
                  <form:form method="post" action="/CDEVendor/AcceptOrReject" enctype="multipart/form-data">
                     <table class="table table-striped table-bordered table-hover"
                        id="itemPoDetails" cellspacing="0" width="100%">
                        <thead>
                           <tr>
                              <th><input type="checkbox" id="checkall"></th>
                              <th>Item Code</th>
                              <th>Item Description</th>
                              <th>Item Type</th>
                              <th>PO Quantity</th>
                              <th>Unit Price</th>
                              <th>Weight</th>
                              <th>UOM</th>
                           </tr>
                        </thead>
                        <tbody>
                           <c:forEach items="${detailslist}" var="detailslist">
                              <tr>
                                 <td><input type="checkbox" id="checkindent" path="itemCode" name="itemcodes[]" value="${detailslist.itemCode}"></td>
                                 <td>${detailslist.itemCode}</td>
                                 <td>${detailslist.itemDescription}</td>
                                 <td>
                                    <c:if test="${detailslist.itemType == 'Y'}">										
                                       <% out.println("Fabrication(CDE-Asia)"); %>
                                    </c:if>
                                    <c:if test="${detailslist.itemType == 'X'}">											
                                       <% out.println("Purchase(CDE-Asia)"); %>
                                    </c:if>
                                    <c:if test="${detailslist.itemType == 'C'}">											
                                       <% out.println("Fabrication(Global)"); %>
                                    </c:if>
                                    <c:if test="${detailslist.itemType == 'P'}">											
                                       <% out.println("Purchase(Global)"); %>
                                    </c:if>
                                 </td>
                                  <td>${detailslist.poqty}</td>
                                 <td>${detailslist.unitPrice}</td>
                                 <td>${detailslist.weight}</td>
                                 <td>${detailslist.uom}</td>
                              </tr>
                           </c:forEach>
                        </tbody>
                     </table>
                     <div class="row" style="padding: 20px;">
                        <div class="col-md-4 form-group">
                           <label>Delivery Charge</label>
                           <input class="form-control"	type="number" min="0" name="delivery_charge" value="">
                        </div>
                        <div class="col-md-4 form-group">
                           <label>Packing Charge</label>
                           <input class="form-control"	type="number" min="0" name="packing_charge" value="">
                        </div>
                        <div class="col-md-4 form-group">
                           <label>Miscellaneous Charge</label>
                           <input class="form-control"	type="number" min="0" name="miscellaneous_charge" value="">
                        </div>
                        <div class="col-md-4 form-group">
                           <label>Comments</label>
                           <textarea rows="3" cols="10" name="comments"></textarea>
                        </div>
                        <div class="col-md-4 form-group">
                           <label>Upload file</label>
                           <input class="form-control"	type="file" name="file">
                        </div>
                        <div class="col-md-4 form-group" style="padding: 3%">
                           <label class="radio-inline">
                           <input class="form-control" type="radio" name="status" value="Accept" checked style="width:55%;">Accept
                           </label>
                           <label class="radio-inline"  style="margin-left: 22px;margin-right:30px;">
                           <input class="form-control" type="radio" name="status" value="Reject" style="width:55%;">Reject
                           </label>
                           <!--  <input class="form-control"
                              type="radio" name="status" value="Accept">Accept
                              <input class="form-control"
                              type="radio" name="status" value="Reject">Reject -->
                           <input class="form-control"
                              type="hidden" name="poNumber" value="${vendorPOListEntity.poNumber}">
                           <button style="margin-top: -38px;" type="submit" name="submit"
                              class="btn  btn-info btn-sm">Submit</button>
                        </div>
                     </div>
                  </form:form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<!-- END PAGE CONTENT-->
<jsp:include page="/vendorfooter.jsp"></jsp:include>
<script>
   $(document).ready(function() {
   	var purchaseTable = $('#itemPoDetails').DataTable({
   
   		pageLength : 10,
   	});
   })
</script>
<script type="text/javascript">
   $('#checkall').click(function(e){
       var table= $(e.target).closest('table');
       $('td input:checkbox',table).prop('checked',this.checked);
   });
</script>