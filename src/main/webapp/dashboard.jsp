<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="/crmheader.jsp"></jsp:include>
<jsp:include page="/crmsidebar.jsp"></jsp:include>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                        <!-- Page/module tite -->
                        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                            <h1 class="h2">Dashboard</h1>
                            <!-- <div class="btn-toolbar mb-2 mb-md-0">
                                <a class="btn btn-sm btn-primary px-3" href="#">
                                    <span data-feather="book"></span>
                                    Some action button
                                </a>
                            </div> -->
                        </div>

                        <!-- Quick Statistic -->
                        <div  class="section-wrap mb-4">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="widget">
                                        <span class="caption">Lead request</span>
                                        <span class="value">330/759</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="widget">
                                        <span class="caption">Customer request</span>
                                        <span class="value">31/0</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                     <div class="widget">
                                         <span class="caption">Customer care request</span>
                                         <span class="value">393/116</span>
                                     </div>
                                 </div>
                            </div>
                         </div><!-- quick stats -->
                         <hr>

                         <!-- first card -->
                         <div class="card">
                             
                             <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="table-responsive">
                                            <h3 class="title">Recent Lead Request</h3>
                                            <table class="table table-bordered table-sm enableDataTable2" id="mainInventoryTable2">
                                                <thead>
                                                  <tr>
                                                    <th>Request No.</th>
                                                    <th>Vendor Name	</th>
                                                   
                                                    <th>Logistic Type</th>
                                                    <th>Weight</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td>LR020/001461</td>
                                                    <td>Prama Manufacturing Pvt. Ltd.</td>
                                                  
                                                    <td>Domestic</td>
                                                    <td></td>
                                                  </tr>
                                                  <tr>
                                                    <td>LR020/001460</td>
                                                    <td>Grindwell Norton Limited</td>
                                                
                                                    <td>Domestic</td>
                                                    <td>60.41 KG</td>
                                                  </tr>
                                                  <tr>
                                                    <td>LR020/001459</td>
                                                    <td>Indiana Gratings Pvt ltd</td>
                                             
                                                    <td>Domestic</td>
                                                    <td>To be confirm..</td>
                                                  </tr>
                                                  <tr>
                                                    <td>LR020/001458</td>
                                                    <td>Rishi Enterprise</td>
                                                  
                                                    <td>Domestic</td>
                                                    <td>5TON</td>
                                                  </tr>
                                                  <tr>
                                                    <td>LR020/001457</td>
                                              
                                                    <td>ALWAR</td>
                                                    <td>Domestic</td>
                                                    <td>5TON</td>
                                                  </tr>
                                                  <tr>
                                                    <td>LR020/001456</td>
                                                    <td>CDE Global Ltd</td>
                                              
                                                    <td>International</td>
                                                    <td>83KGS</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="table-responsive">
                                            <h3 class="title">Recent Customer Request</h3>
                                           <table class="table table-bordered table-sm enableDataTable">
                                               <thead>
                                                 <tr>
                                                   <th>Request No.</th>
                                                   <th>Vendor Name	</th>
                                                  
                                                   <th>Logistic Type</th>
                                                   <th>Weight</th>
                                                 </tr>
                                               </thead>
                                               <tbody>
                                                 <tr>
                                                   <td>LR020/001461</td>
                                                   <td>Prama Manufacturing Pvt. Ltd.</td>
                                                 
                                                   <td>Domestic</td>
                                                   <td></td>
                                                 </tr>
                                                 <tr>
                                                   <td>LR020/001460</td>
                                                   <td>Grindwell Norton Limited</td>
                                               
                                                   <td>Domestic</td>
                                                   <td>60.41 KG</td>
                                                 </tr>
                                                 <tr>
                                                   <td>LR020/001459</td>
                                                   <td>Indiana Gratings Pvt ltd</td>
                                            
                                                   <td>Domestic</td>
                                                   <td>To be confirm..</td>
                                                 </tr>
                                                 <tr>
                                                   <td>LR020/001458</td>
                                                   <td>Rishi Enterprise</td>
                                                 
                                                   <td>Domestic</td>
                                                   <td>5TON</td>
                                                 </tr>
                                                 <tr>
                                                   <td>LR020/001457</td>
                                             
                                                   <td>ALWAR</td>
                                                   <td>Domestic</td>
                                                   <td>5TON</td>
                                                 </tr>
                                                 <tr>
                                                   <td>LR020/001456</td>
                                                   <td>CDE Global Ltd</td>
                                             
                                                   <td>International</td>
                                                   <td>83KGS</td>
                                                 </tr>
                                               </tbody>
                                             </table>
                                        </div>
                                        
                                    </div>
                                </div>
                                
                                

                             </div>
                         </div>
                         <!-- /first card -->
                           <!-- first card -->
                           <div class="card">
                             
                            <div class="card-body">
                               <div class="row">
                                   <div class="col-md-6">
                                    <figure class="highcharts-figure">
                                        <div id="container"></div>
                                       
                                    </figure>

                                   </div>
                                   <div class="col-md-6">
                                    <figure class="highcharts-figure">
                                        <div id="container2"></div>
                                       
                                    </figure>
                                       
                                   </div>
                               </div>
                               
                               

                            </div>
                        </div>
                        <!-- /first card -->
                       
                    </main>
                </div>
            </div>
        </section>
        
        <jsp:include page="/crmfooter.jsp"></jsp:include>