<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
     <jsp:include page="/crmheader.jsp"></jsp:include>
<jsp:include page="/crmsidebar.jsp"></jsp:include>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                        <!-- Page/module tite -->
                        <div
                            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                            <h1 class="h2"> Customer Management</h1>
                            <!-- <div class="btn-toolbar mb-2 mb-md-0">
                                <div class="btn-group mr-2">
                                    <a href=""  class="btn btn-primary"> Add new <span data-feather="plus"></span></a>
                                </div>
                            </div> -->
                        </div>
                    
                        <div class="card">
                            <div class="card-body">
                                <table class="table enableDataTable">
                                    <thead>
                                        <tr>
                                            <th>Customer Id</th>
                                            <th>Customer Name</th>
                                            <th>Email</th>
                                            <th>Contact Number</th>
                                          
                                            
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                ld 001
                                            </td>
                                            <td>
                                                John Doe
                                            </td>
                                            <td>
                                                john@gmail.com
                                            </td>
                                            <td>
                                                98723642820
                                            </td>
                                           
                                            <td>
                                                <a href="editcustomer" class="badge-pill badge-primary" >Edit</a>
                                                <a href="#" class="badge-pill badge-primary"  data-toggle="modal" data-target="#exitView" >Upload KYC</a>
                                               
                                             
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td>
                                                ld 001
                                            </td>
                                            <td>
                                                John Doe
                                            </td>
                                            <td>
                                                john@gmail.com
                                            </td>
                                            <td>
                                                98723642820
                                            </td>
                                           
                                            <td>
                                                <a href="editcustomer" class="badge-pill badge-primary" >Edit</a>
                                                <a href="#" class="badge-pill badge-primary"  data-toggle="modal" data-target="#exitView" >Upload KYC</a>
                                               
                                            </td>
                                        </tr>
                                       
                                       
                                       
                                       
                                        
                                    </tbody>
                                    <!-- <tfoot>
                                        <tr>
                                            <th>Lead Id</th>
                                            <th>Lead Name</th>
                                            <th>Gmail</th>
                                            <th>Contact Number</th>
                                           
                                        
                                            <th>Action</th>
                                        </tr>
                                    </tfoot> -->
                                </table>
                            </div>
                        </div>
                    
                    </main>
                </div>
            </div>
        </section>
         <jsp:include page="/crmfooter.jsp"></jsp:include>