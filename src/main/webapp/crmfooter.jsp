<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- chatr.js -->
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="<c:url value="/resources/assets/vendor/jquery-3.3.1.min.js" />">
        </script>
        <script src="<c:url value="/resources/assets/vendor/popper.min.js" />"></script>
        <script src="<c:url value="/resources/assets/core/bootstrap.js" />"></script>
        <!-- datepicker -->
        <script src="<c:url value="/resources/assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" />"></script>
        <!-- select2 -->
        <script src="<c:url value="/resources/assets/plugins/select2/js/select2.full.js" />"></script>
        <!-- Icons -->
        <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
        <!-- multiselect -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js">
        </script>
        <!--  -->
        <script src="<c:url value="/resources/assets/script/dynamicfield.js" />"></script>
        <!-- Init js -->
        <script src="<c:url value="/resources/assets/script/dashboard.js" />"></script>
        <!-- Data table Buttons -->
        <script src="https://auto-clinic.in/cde_ims/assets/vendors/DataTables/datatables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
       <!-- group bar -->
<script>
    Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    credits: {
    enabled: false
  },
    title: {
        text: 'This Year Earning & Expences'
    },
    subtitle: {
        // text: 'Source: WorldClimate.com'
    },
    xAxis: {
        categories: [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Pr1',
        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

    }, {
        name: 'pr2',
        data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

    }]
});
</script>
  <!-- group bar -->
  <script>
    Highcharts.chart('container2', {
    chart: {
        type: 'column'
    },
    credits: {
    enabled: false
  },
    title: {
        text: 'This Year Earning & Expences'
    },
    subtitle: {
        // text: 'Source: WorldClimate.com'
    },
    xAxis: {
        categories: [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Pr1',
        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

    }, {
        name: 'pr2',
        data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

    }]
});
</script>
<!-- data table part 2 -->
<!-- <script type="text/javascript">
    $(document).ready(function() {
        $('#mainInventoryTable2').DataTable( {
            dom: 'lBfrtip',
             pageLength : 5,
            //buttons: ['excel']
            // buttons: [ { extend: 'excel', text: 'Download in Excel Format' } ]
        } );
        $('#purchaseOrderTable').DataTable( {
            dom: 'lBfrtip',
            pageLength : 5,
            //buttons: ['excel']
            // buttons: [ { extend: 'excel', text: 'Download in Excel Format' } ]
        } );
        $('#itemmaster_table').DataTable( {
            dom: 'lBfrtip',
            // buttons: ['excel']
            // buttons: [ { extend: 'excel', text: 'Download in Excel Format' } ]
        } );
        $('#giftTable').DataTable( {
            dom: 'lBfrtip',
            pageLength : 5,
            //buttons: ['excel']
            // buttons: [ { extend: 'excel', text: 'Download in Excel Format' } ]
        } );
           $('#indentTable').DataTable( {
            dom: 'lBfrtip',
            //buttons: ['excel']
            // buttons: [ { extend: 'excel', text: 'Download in Excel Format' } ]
        } );
        $('#indentProcessedTable').DataTable( {
            dom: 'lBfrtip',
            //buttons: ['excel']
            // buttons: [ { extend: 'excel', text: 'Download in Excel Format' } ]
        } );
        $('#stdItemTable').DataTable( {
            dom: 'lBfrtip',
            // "bLengthChange": false,
                // "bFilter": false,
                // "bInfo": false,
                // "bPaginate": false
            //buttons: ['excel']
            // buttons: [ { extend: 'excel', text: 'Download in Excel Format' } ]
        } );
        $('#nonStdItemTable').DataTable( {
            dom: 'lBfrtip',
            //buttons: ['excel']
            // "bLengthChange": false,
                // "bFilter": false,
                // "bInfo": false,
            // buttons: [ { extend: 'excel', text: 'Download in Excel Format' } ]
        } );
        $('#itemmaster-table-project').DataTable( {
            dom: 'lBfrtip',
            //buttons: ['excel']
            // buttons: [ { extend: 'excel', text: 'Download in Excel Format' } ]
        } );
        $('#itemmaster-table').DataTable( {
            dom: 'lBfrtip',
            //buttons: ['excel']
            // buttons: [ { extend: 'excel', text: 'Download in Excel Format' } ]
        } );
        $('#grnListTable').DataTable( {
            dom: 'lBfrtip',
            //buttons: ['excel']
            // buttons: [ { extend: 'excel', text: 'Download in Excel Format' } ]
        } );
        $('#mainStoreTable').DataTable( {
            dom: 'lBfrtip',
            //buttons: ['excel']
            // buttons: [ { extend: 'excel', text: 'Download in Excel Format' } ]
        } );
} );
</script> -->
<!-- data table -->
<script>
    $(document).ready(function() {
        $('.enableDataTable').DataTable( {
            "scrollY":        "350px",
            "scrollCollapse": true,
            "paging":         false
        } );
    } );
</script>
<script>
    $(document).ready(function() {
        $('.enableDataTable2').DataTable( {
            "scrollY":        "350px",
            "scrollCollapse": true,
            "paging":         false
        } );
    } );
</script>
    </body>

</html>