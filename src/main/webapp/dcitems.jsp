<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<jsp:include page="/vendorheader.jsp"></jsp:include>
<jsp:include page="/vendorsidebar.jsp"></jsp:include>
<%-- welcome <%=session.getAttribute("uname") %> --%>

<div class="content-wrapper">
	<!-- START PAGE CONTENT-->
	<div class="page-content fade-in-up">
		<div class="row">

			
			<!-- polist -->
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-head" style="background-color: #0077c0;">
						<div class="ibox-title" style="color: white;">DC Item List</div>
						<!-- <div class="ibox-title"><input type="text" id="indentSearchDiv" style="height: 25px; font-size: 11px;" placeholder="Search ....." size="50px"/></div> -->
					</div>
                   
					<div class="ibox-body">
						<table class="table table-striped table-bordered table-hover"
							id="poListDataTable" cellspacing="0" width="100%">
							<thead>
								<tr>
								
									<th>DC Number</th>
									<th>Indent Number</th>
									<th>Project Number</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${vpolist}" var="vpolist">
									<tr>
										<td>${vpolist.deliverychallan}</td>
										<td>${vpolist.indentID}</td>
										<td>${vpolist.projectID}</td>
                                        <td>
                                         <form:form id="poTableform" method="post" action="/CDEVendor/dcitemdetails" modelAttribute="viewPODetails">
                                         	<form:input type="hidden" path="deliverychallan"  name="deliverychallan" value="${vpolist.deliverychallan}" />
                                       		<button	type="submit" class="btn btn-info btn-sm">Details</button>
											
												
                                          </form:form>
                                         
                                        
                                        </td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
                 
				</div>
			</div>

		</div>
	</div>
	<!-- END PAGE CONTENT-->

<jsp:include page="/vendorfooter.jsp"></jsp:include>
<script>
	$(document).ready(function() {
		var purchaseTable = $('#pricingDataTable').DataTable({
			
			pageLength : 5,
		});
	})
	
	$(document).ready(function() {
		var purchaseTable = $('#poListDataTable').DataTable({
			
			pageLength : 5,
		});
	})
	
	function editList(itemCode){	
		alert(itemCode);
		document.getElementById("editTableform").submit();
	}
	
	
	
	function poList(indentID,projectID,poNumber){	
		alert(poNumber);
		document.getElementById("poTableform").submit();
	}
	</script>