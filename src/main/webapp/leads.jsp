<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <jsp:include page="/crmheader.jsp"></jsp:include>
<jsp:include page="/crmsidebar.jsp"></jsp:include>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                        <!-- Page/module tite -->
                        <div
                            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                            <h1 class="h2"> Lead Management</h1>
                            <div class="btn-toolbar mb-2 mb-md-0">
                                <div class="btn-group mr-2">
                                    <a href="newleads"  class="btn btn-primary"> Add new <span data-feather="plus"></span></a>
                                </div>
                            </div>
                        </div>

                        <!-- Lead widget -->
                        <div  class="section-wrap mb-4">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="widget">
                                        <span class="caption">Lead created</span>
                                        <span class="value">1,235</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="widget">
                                        <span class="caption">Quotation shared</span>
                                        <span class="value">150</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="widget">
                                        <span class="caption">Quotation approved</span>
                                        <span class="value">45</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="widget">
                                        <span class="caption">Quotation rejected</span>
                                        <span class="value">105</span>
                                    </div>
                                </div>
                            </div>
                         </div><!-- Lead widget -->
                    
                        <div class="card">
                            <div class="card-body">
                                <table class="table enableDataTable">
                                    <thead>
                                        <tr>
                                            <th>Lead Id</th>
                                            <th>Lead Name</th>
                                            <th>Email</th>
                                            <th>Contact number</th>
                                            <th>Lead Source</th>
                                            <th>Status</th>
                                            
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>ld 001</td>
                                            <td>John Doe</td>
                                            <td>john@gmail.com</td>
                                            <td>98723642820</td>
                                            <td>Online</td>
                                            
                                            <td><span class="badge-pill badge-secondary">New</span></td>
                                            <td>
                                                <a href="edit_lead.html" class="badge-pill badge-primary" >Edit</a>
                                                <a href="#" class="badge-pill badge-primary" data-toggle="modal" data-target="#changeLeadStatus">Change status</a>
                                                <a href="javascript:void(0);" class="badge-pill badge-danger">Delete</a>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td>ld 001</td>
                                            <td>John Doe</td>
                                            <td>john@gmail.com</td>
                                            <td>98723642820</td>
                                            <td>Online</td>
                                            
                                            <td><span class="badge-pill badge-secondary">New</span></td>
                                            <td>
                                                <a href="edit_lead.html" class="badge-pill badge-primary" >Edit</a>
                                                <a href="#" class="badge-pill badge-primary" data-toggle="modal" data-target="#changeLeadStatus">Change status</a>
                                                <a href="javascript:void(0);" class="badge-pill badge-danger">Delete</a>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td>ld 001</td>
                                            <td>John Doe</td>
                                            <td>john@gmail.com</td>
                                            <td>98723642820</td>
                                            <td>Online</td>
                                            
                                            <td><span class="badge-pill badge-secondary">New</span></td>
                                            <td>
                                                <a href="edit_lead.html" class="badge-pill badge-primary" >Edit</a>
                                                <a href="#" class="badge-pill badge-primary" data-toggle="modal" data-target="#changeLeadStatus">Change status</a>
                                                <a href="javascript:void(0);" class="badge-pill badge-danger">Delete</a>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td>ld 001</td>
                                            <td>John Doe</td>
                                            <td>john@gmail.com</td>
                                            <td>98723642820</td>
                                            <td>Online</td>
                                            
                                            <td><span class="badge-pill badge-secondary">New</span></td>
                                            <td>
                                                <a href="edit_lead.html" class="badge-pill badge-primary" >Edit</a>
                                                <a href="#" class="badge-pill badge-primary" data-toggle="modal" data-target="#changeLeadStatus">Change status</a>
                                                <a href="javascript:void(0);" class="badge-pill badge-danger">Delete</a>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td>ld 001</td>
                                            <td>John Doe</td>
                                            <td>john@gmail.com</td>
                                            <td>98723642820</td>
                                            <td>Online</td>
                                            
                                            <td><span class="badge-pill badge-secondary">New</span></td>
                                            <td>
                                                <a href="edit_lead.html" class="badge-pill badge-primary" >Edit</a>
                                                <a href="#" class="badge-pill badge-primary" data-toggle="modal" data-target="#changeLeadStatus">Change status</a>
                                                <a href="javascript:void(0);" class="badge-pill badge-danger">Delete</a>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td>ld 001</td>
                                            <td>John Doe</td>
                                            <td>john@gmail.com</td>
                                            <td>98723642820</td>
                                            <td>Online</td>
                                            
                                            <td><span class="badge-pill badge-secondary">New</span></td>
                                            <td>
                                                <a href="edit_lead.html" class="badge-pill badge-primary" >Edit</a>
                                                <a href="#" class="badge-pill badge-primary" data-toggle="modal" data-target="#changeLeadStatus">Change status</a>
                                                <a href="javascript:void(0);" class="badge-pill badge-danger">Delete</a>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td>ld 001</td>
                                            <td>John Doe</td>
                                            <td>john@gmail.com</td>
                                            <td>98723642820</td>
                                            <td>Online</td>
                                            
                                            <td><span class="badge-pill badge-secondary">New</span></td>
                                            <td>
                                                <a href="edit_lead.html" class="badge-pill badge-primary" >Edit</a>
                                                <a href="#" class="badge-pill badge-primary" data-toggle="modal" data-target="#changeLeadStatus">Change status</a>
                                                <a href="javascript:void(0);" class="badge-pill badge-danger">Delete</a>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td>ld 001</td>
                                            <td>John Doe</td>
                                            <td>john@gmail.com</td>
                                            <td>98723642820</td>
                                            <td>Online</td>
                                            
                                            <td><span class="badge-pill badge-secondary">New</span></td>
                                            <td>
                                                <a href="edit_lead.html" class="badge-pill badge-primary" >Edit</a>
                                                <a href="#" class="badge-pill badge-primary" data-toggle="modal" data-target="#changeLeadStatus">Change status</a>
                                                <a href="javascript:void(0);" class="badge-pill badge-danger">Delete</a>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td>ld 001</td>
                                            <td>John Doe</td>
                                            <td>john@gmail.com</td>
                                            <td>98723642820</td>
                                            <td>Online</td>
                                            
                                            <td><span class="badge-pill badge-secondary">New</span></td>
                                            <td>
                                                <a href="edit_lead.html" class="badge-pill badge-primary" >Edit</a>
                                                <a href="#" class="badge-pill badge-primary" data-toggle="modal" data-target="#changeLeadStatus">Change status</a>
                                                <a href="javascript:void(0);" class="badge-pill badge-danger">Delete</a>
                                            </td>
                                        </tr> 

                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <!-- Changes status : LEad modal -->
                        <div class="modal fade" id="changeLeadStatus" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="changeLeadStatusTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalCenterTitle">Change lead status</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="">Change lead status to</label>
                                        <select class="form-control">
                                            <option>Lead created</option>
                                            <option>Customer follow-up</option>
                                            <option value="">Sample requested</option>
                                            <option value="">Quotation shared</option>
                                            <option value="">Asked for quotation</option>
                                            <option value="">Quotation approved</option>
                                            <option value="">Quotation rejected</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Change</button>
                                </div>
                            </div>
                            </div>
                        </div>
                    
                    </main>
                </div>
            </div>
        </section>
        
          <jsp:include page="/crmfooter.jsp"></jsp:include>