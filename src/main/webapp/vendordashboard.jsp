<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <jsp:include page="/vendorheader.jsp"></jsp:include>  
     <jsp:include page="/vendorsidebar.jsp"></jsp:include>  
     <!DOCTYPE html>
    <div class="content-wrapper" id="pageContentDiv">
            <!-- START PAGE CONTENT-->
			<div class="page-content fade-in-up">
			<form:form id="indentmasterform" action="/CDEVendor/viewdashboard"  method="post" modelAttribute="loginModel">
			   <div class="row">
			      <div class="col-md-4">
			      	<label><b>From Date</b></label>
			      	<c:if test = "${fromdate == '' }">
			      		<input type="date" class="form-control" name="from_date" style="padding-left: 18px;height: 36px;border-radius: 10px;">
			      	</c:if>
			      	<c:if test = "${fromdate != '' }">
			      		<input type="date" class="form-control" name="from_date" value="${fromdate}" style="padding-left: 18px;height: 36px;border-radius: 10px;">
			      	</c:if>
			      </div>
			      <div class="col-md-4">
			      	<label><b>To Date</b></label>
			      	<c:if test = "${todate == '' }">
			      		<input type="date" class="form-control" name="to_date" style="padding-left: 18px;height: 36px;border-radius: 10px;">
			      	</c:if>
			      	<c:if test = "${todate != '' }">
			      		<input type="date" class="form-control" name="to_date" value="${todate}" style="padding-left: 18px;height: 36px;border-radius: 10px;">
			      	</c:if>
			      	
			      </div>
			      <div class="col-md-4"><button type="submit" class="btn btn-info" value="${todate}" style="margin-top: 28px;border-radius: 9px;">Filter</button></div>
			   </div>
			</form:form><br>
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="ibox bg-success color-white widget-stat">
                            <div class="ibox-body">
                            <%-- <c:forEach items="${meindent}" var="meindent">
                                <h2 class="m-b-5 font-strong">${meindent.indent_count}</h2>
                            </c:forEach> --%>
                            <c:forEach items="${porcv}" var="porcv">
                            	<h2 class="m-b-5 font-strong">${porcv.porcv}</h2>
                            </c:forEach>
                                <div class="m-b-5">Total PO Received</div><i class="ti-shopping-cart widget-stat-icon"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="ibox bg-info color-white widget-stat">
                            <div class="ibox-body">
                               <%--  <c:forEach items="${mepoissued}" var="mepoissued">
                                <h2 class="m-b-5 font-strong">${mepoissued.poissue_count}</h2>
                            </c:forEach> --%>
                           <c:forEach items="${poaccept}" var="poaccept">
                            	<h2 class="m-b-5 font-strong">${poaccept.poaccept}</h2>
                            </c:forEach>
                                <div class="m-b-5">Total PO Accepted</div><i class="ti-bar-chart widget-stat-icon"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="ibox bg-warning color-white widget-stat">
                            <div class="ibox-body">
                               <%--  <c:forEach items="${megrnraise}" var="megrnraise">
                                <h2 class="m-b-5 font-strong">${megrnraise.grnraised_count}</h2>
                            </c:forEach> --%>
                            <c:forEach items="${poreject}" var="poreject">
                            	<h2 class="m-b-5 font-strong">${poreject.poreject}</h2>
                            </c:forEach>
                                <div class="m-b-5">Total PO Rejected</div><i class="fa fa-money widget-stat-icon"></i>
                            </div>
                        </div>
                    </div>
                    <%-- <div class="col-lg-3 col-md-6">
                        <div class="ibox bg-danger color-white widget-stat">
                            <div class="ibox-body">
                               <c:forEach items="${medespatch}" var="medespatch">
                                <h2 class="m-b-5 font-strong">${medespatch.despatched_count}</h2>
                            </c:forEach>
                            <h2 class="m-b-5 font-strong">5</h2>
                                <div class="m-b-5">Total Items</div><i class="ti-user widget-stat-icon"></i>
                            </div>
                        </div>
                    </div> --%>
                </div>
				
				                <div class="row">
                    <div class="col-lg-6">
                        <div class="ibox">
                            <div class="ibox-head" style="height: 28px; background-color: #0077c0; color: white;">
                                <div class="ibox-title">Price Request of Items</div>
                                <!-- <div class="ibox-title"><input type="text" id="inventorySearchDiv" style="height: 20px; font-size: 11px;" placeholder="Search" size="50px"/></div> -->
                            </div>
                            <div style="" class="ibox-body">
                                <table class="table table-striped table-bordered table-hover" id="mainInventoryTable" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Item Code</th>
                                            <th>Item Description</th>
                                            <!-- <th>Unit Price</th> -->
											<th>Weight</th>
											<th>UOM</th>
                                        </tr>
                                    </thead>
                                    <tbody>
								  <c:forEach items="${pricinglistlatest}" var="pricinglistlatest">
									<tr><td>${pricinglistlatest.itemCode}</td>
	      								<td>${pricinglistlatest.itemDescription}</td>
		                 				<%-- <td>${pricinglistlatest.unitPrice}</td> --%>  
		                 				<td>${pricinglistlatest.weight}</td>
		                 				<td>${pricinglistlatest.uom}</td>
		                 				</tr>
		                 				</c:forEach>  
									</tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-6">
                		<div class="ibox">
                    		<div class="ibox-head" style="height: 28px; background-color: #0077c0; color: white;">
                        		<div class="ibox-title"> Latest PO Received </div>
								<!-- <div class="ibox-title"><input type="text" id="purchaseSearchDiv" style="height: 20px; font-size: 11px;" placeholder="Search " size="50px"/></div> -->
                    		</div>
                    		<div class="ibox-body">
                        		<table class="table table-striped table-bordered table-hover" id="purchaseOrderTable" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Indent Number</th>
									<th>Project Number</th>
                                    <th>PO Number</th>
                                </tr>
                            </thead>
                            <tbody>
                            	<c:forEach items="${vpolistlatest}" var="vpolistlatest">
									<tr><td>${vpolistlatest.indentID}</td>
	      								<td>${vpolistlatest.projectID}</td>
	      								<td>${vpolistlatest.poNumber}</td>
		                 				</tr>
		                 				</c:forEach>
									</tbody>
                            </tbody>
                            
                        </table>
                    		</div>
                		</div>
                	</div>
                 
                </div>
				
				

            </div>
           <jsp:include page="/vendorfooter.jsp"></jsp:include>  

<script type="text/javascript">

$(document).ready(function() {
	 $('#mainInventoryTable').DataTable( {
        //dom: 'lBfrtip',
         pageLength : 5,
        //buttons: ['excel']
        //buttons: [ { extend: 'excel', text: 'Download in Excel Format' } ]
    } );
	 
	 
})


$(document).ready(function() {
	 $('#purchaseOrderTable').DataTable();
} )
</script>

   