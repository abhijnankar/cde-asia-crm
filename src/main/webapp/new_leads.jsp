<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
        <jsp:include page="/crmheader.jsp"></jsp:include>
<jsp:include page="/crmsidebar.jsp"></jsp:include>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                        <!-- Page/module tite -->
                        <div
                            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                            <h1 class="h2"> ADD New Lead</h1>
                            <!-- <div class="btn-toolbar mb-2 mb-md-0">
                                <div class="btn-group mr-2">
                                    <a href="javascrip:void(0);" data-toggle="modal" data-target="#newEmployeeExit" class="btn btn-primary"> Add new <span data-feather="plus"></span></a>
                                </div>
                            </div> -->
                        </div>
                    
                        <div class="card">
                            <div class="card-body">
                                <form id="regForm" action="/action_page.php">
                                   
                                    <!-- One "tab" for each step in the form: -->
                                    <div class="tab">
                                      <h1 class="h2">Basic Info:</h1>
                                      <div class="conatiner">
                                          <div class="row no-gutters">
                                            <div class="col-md-8">
                                              <div class="row">
                                                <div class="col-md-3">
                                                  <div class="form-group">
                                                      <label for="usr">Lead Name:</label>
                                                      <input type="text" class="form-control">
                                                  </div>
                                              </div>
                                              <div class="col-md-3">
                                                  <div class="form-group">
                                                      <label for="usr">Email:</label>
                                                      <input type="email" class="form-control">
                                                  </div>
                                              </div>
                                              <div class="col-md-3">
                                                  <div class="form-group">
                                                      <label for="usr">Contact Number:</label>
                                                      <input type="email" class="form-control">
                                                  </div>
                                              </div>
                                              <div class="col-md-3">
                                                  <div class="form-group">
                                                      <label class="form-control-label" for="customer">Lead Source :</label>
                                                      <select class="form-control" data-toggle="select">
                                                          <option>--Select Lead Source--</option>
                                                          <option>Online</option>
                                                          <option>Offline</option>
                                                          <option>Advertisement</option>
                                                      </select>
                                                  </div>
                                              </div>
                                              <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="comment">Address:</label>
                                                    <textarea class="form-control" rows="1" id="adress"></textarea>
                                                </div>
                                            </div>
                                              <div class="col-md-3">
                                                  <div class="form-group">
                                                      <label class="form-control-label" for="customer">Status :</label>
                                                      <select class="form-control" data-toggle="select">
                                                          <option>--Select Status--</option>
                                                          <option>New</option>
                                                          <option>Mature</option>
                                                      </select>
                                                  </div>
                                              </div>
                                              <div class="col-md-12 mb-3" >
                                                <div class="form-group">
                                                    <label for="comment">Remarks:</label>
                                                    <textarea class="form-control" rows="3" id="Remarks"></textarea>
                                                </div>
                                            </div>
                                              </div>
                                            </div>
                                          
                                            
                                          </div>
                                      </div>
                                    </div>
                                    <!-- detaisl info -->
                                    <div class="tab">
                                      <h1 class="h2">Details Info:</h1>
                                      <div class="conatiner">
                                        <div class="row no-gutters">
                                         
                                          <div class="col-md-8">
                                            <div class="apend">
                                              <div class="row">
                                                <div class="col">
                                                  <div class="form-group">
                                                    <label for="usr">Sample Name:</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                                 </div>
                                                 <div class="col">
                                                  <label for="usr">Upload Sample:</label>
                                                  <div class="custom-file">
                                                      <input type="file" class="custom-file-input" multiple id="customFile">
                                                      <label class="custom-file-label" for="customFile">Choose file</label>
                                                  </div>
                                              </div>
                                              <div class="col">
                                                <a class="btn btn-primary mt-4 add_button3"
                                                href="javascript:void(0);">
                                                +
                                            </a>
                                              </div>
                                              </div>
                                              <!-- append row end -->
                                            </div>
                                            <div class="row">
                                               

                                          <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="comment">Details Requirement:</label>
                                                <textarea class="form-control" rows="8" id="adress"></textarea>
                                            </div>
                                        </div>
                                       
                                            </div>
                                          </div>
                                       
                                          
                                        </div>
                                    </div>


                                    </div>
                                    
                                    <div style="overflow:auto;">
                                      <div class="mt-3" style="float:left;">
                                        <button type="button"  class="btn btn-primary" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                                      
                                        <button type="button"  class="btn btn-primary" id="nextBtn" onclick="nextPrev(1)">Next</button>
                                        <button type="button"  class="btn btn-primary" id="nextBtn" >Save</button>
                                      </div>
                                    </div>
                                    <!-- Circles which indicates the steps of the form: -->
                                    <div style="text-align:center;margin-top:40px;">
                                      <span class="step"></span>
                                      <span class="step"></span>
                                     
                                    </div>
                                  </form>
                                    
                          </div>
            </div>
        </section>
      <jsp:include page="/crmfooter.jsp"></jsp:include>