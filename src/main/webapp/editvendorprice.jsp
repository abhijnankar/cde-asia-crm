<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<jsp:include page="/vendorheader.jsp"></jsp:include>
<jsp:include page="/vendorsidebar.jsp"></jsp:include>
<%-- welcome <%=session.getAttribute("uname") %> --%>

<div class="content-wrapper">
	<!-- START PAGE CONTENT-->
	<div class="page-content fade-in-up">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox">

					<div class="ibox-head" style="background-color: #512DA8;">
						<div class="ibox-title" style="color: white;">Edit Pricing</div>
						<!-- <div class="ibox-title"><input type="text" id="stdItemSearchDiv" style="height: 25px; font-size: 11px;" placeholder="Search ....." size="50px"/></div> -->
					</div>
					<form:form methot="post" action="/CDEVendor/updateVendorPrice" enctype="multipart/form-data">
						<div class="ibox-body" style="padding: 20px;">
							<div class="row">
								<div class="col-md-4 form-group">
									<label>Item Code</label>
									<c:forEach items="${editlist}" var="editlist">
										<input class="form-control" type="text" readonly="true"
											value="${editlist.itemCode}" name="itmCode">
									</c:forEach>

								</div>


								<div class="col-md-4 form-group">

									<label>Item Description</label>
									<c:forEach items="${editlist}" var="editlist">
										<input class="form-control" type="text" readonly="true"
											value="${editlist.itemDescription}" name="itemDescription">
									</c:forEach>
								</div>
								<div class="col-md-4 form-group">
									<label> Item Type</label>
									<c:forEach items="${editlist}" var="editlist">
										<c:if test="${editlist.itemType == 'Y'}">
											<input class="form-control" type="text" readonly="true"
												value="Fabrication(CDE-Asia)" name="itemType">
										</c:if>
										<c:if test="${editlist.itemType == 'X'}">
											<input class="form-control" type="text" readonly="true"
												value="Purchase(CDE-Asia)" name="itemType">
										</c:if>
										<c:if test="${editlist.itemType == 'C'}">
											<input class="form-control" type="text" readonly="true"
												value="Fabrication(Global)" name="itemType">
										</c:if>
										<c:if test="${editlist.itemType == 'P'}">
											<input class="form-control" type="text" readonly="true"
												value="Purchase(Global)" name="itemType">
										</c:if>
										
										
									</c:forEach>


								</div>
								<div class="col-md-4 form-group" hidden>

									<label> Unit Price</label>
									<c:forEach items="${editlist}" var="editlist">
										<input class="form-control" type="text" readonly="true"
											value="${editlist.unitPrice}" name="unitPrice">
									</c:forEach>
								</div>
								<div class="col-md-4 form-group">

									<label> Vendor Price </label> 
									<c:forEach items="${editlist}" var="editlist">
										<input class="form-control"
										type="text" name="vendorPrice" value="${editlist.vendorprice}">
									</c:forEach>
								</div>
								<div class="col-md-4 form-group">
									<label> Lead Time(In Days) </label> 
									<c:forEach items="${editlist}" var="editlist">
										<input class="form-control"	type="number" min="0" name="lead_time" value="${editlist.leadtime}">
									</c:forEach>
								</div>
								<div class="col-md-4 form-group">
									<label> Upload File </label> 
										<input class="form-control" type="file" min="0" name="file" value="">
										<c:forEach items="${editlist}" var="editlist">
											 <c:if test="${(not empty editlist.uploadfile) && (editlist.uploadfile != 'null') }">
											 <br>	<p>Previous Uploaded file : 
                                        		<a download target="_blank" href="downloads/${editlist.uploadfile}">${editlist.uploadfile}</a></p>
                                        		<input type="hidden" name="prev_file" value="${editlist.uploadfile}">
                                        </c:if>
                                        <c:if test="${editlist.uploadfile == 'null'}">
											 
                                        		<input type="hidden" name="prev_file" value="">
                                        </c:if>
                                        
										</c:forEach>
										
								</div>
								<div class="col-md-4">
									<button style="margin-top: 25px;" type="submit" name="submit"
										class="btn  btn-info">Submit</button>

								</div>
							</div>

						</div>
					</form:form>



				</div>




			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT-->

<jsp:include page="/vendorfooter.jsp"></jsp:include>
