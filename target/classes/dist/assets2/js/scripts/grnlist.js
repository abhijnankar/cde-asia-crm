$(function() {

	// data table
	$('#itemmaster-table').DataTable({
		pageLength: 10,
		order: [[ 1, "desc" ]],
		"oLanguage": {
		  "sSearch": "Search By Vendor Name or PO Number:"
		},
		"columnDefs": [{
			"targets": [0,10],
			"orderable": false
		}]
	});
	
	// 
	var rowstable = $('#itemmaster-table tr');
	
	$("input[data-select='all']").change(function(){
        $(this).prop('checked') 
            ? rowstable.find('.mail-check-project').prop('checked',true)
            : rowstable.find('.mail-check-project').prop('checked',false)        
    });
});