$(function() {
	// select box
	$(".select2_job_number").select2({
        placeholder: "Select Job Number",
        allowClear: true
    });
    $(".select2_plant_item").select2({
        placeholder: "Select Plant Item",
        allowClear: true
    });
    $(".select2_sub_assembly_code").select2({
        placeholder: "Select Sub Assembly Code",
        allowClear: true
    });
    $(".select2_component_code").select2({
        placeholder: "Select Component Code",
        allowClear: false
    });
    $(".select2_measurement_unit").select2({
        placeholder: "Select Measurement Unit",
        allowClear: false
    });
});