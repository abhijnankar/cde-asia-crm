$(function() {
	
	// doughnut chart example

    var data_suppliers = {
	  datasets: [
		{
		  data: [100],
		  backgroundColor: [
			"#23b7e5"
		  ],
		  hoverBackgroundColor: [
			"#23b7e5"
		  ]
		}]
	};

    var data_suppliers_Options = {
        responsive: true,
        legend: {
		  display: false
		},
		tooltips: {
		  enabled:false
		},
		cutoutPercentage: 80
    };

    var ctx_suppliers = document.getElementById("suppliers_chart").getContext("2d");
    new Chart(ctx_suppliers, {type: 'doughnut', data: data_suppliers, options:data_suppliers_Options});
    
    //
    var data_contracted = {
	  datasets: [
		{
		  data: [61,39],
		  backgroundColor: [
			"#2ecc71",
			"#eeeeee"
		  ],
		  hoverBackgroundColor: [
			"#2ecc71",
			"#eeeeee"
		  ]
		}]
	};

    var data_contracted_Options = {
        responsive: true,
        legend: {
		  display: false
		},
		tooltips: {
		  enabled:false
		},
		cutoutPercentage: 80
    };

    var ctx_contracted = document.getElementById("contracted_chart").getContext("2d");
    new Chart(ctx_contracted, {type: 'doughnut', data: data_contracted, options:data_contracted_Options});
    
    //
    var data_unlisted = {
	  datasets: [
		{
		  data: [39,61],
		  backgroundColor: [
			"#e74c3c",
			"#eeeeee"
		  ],
		  hoverBackgroundColor: [
			"#e74c3c",
			"#eeeeee"
		  ]
		}]
	};

    var data_unlisted_Options = {
        responsive: true,
        legend: {
		  display: false
		},
		tooltips: {
		  enabled:false
		},
		cutoutPercentage: 80
    };

    var ctx_unlisted = document.getElementById("unlisted_chart").getContext("2d");
    new Chart(ctx_unlisted, {type: 'doughnut', data: data_unlisted, options:data_unlisted_Options});
    
    Morris.Line({
        element: 'morris_line_chart',
        data: [
            { y: '2008', a: 90, b: 40 ,c: 30 },
            { y: '2009', a: 105, b: 65 ,c: 40 },
            { y: '2010', a: 30, b: 40 ,c: 50 },
            { y: '2011', a: 105, b: 65 ,c: 60 },
            { y: '2012', a: 100, b: 90 ,c: 70 } ],
        xkey: 'y',
        ykeys: ['a', 'b', 'c'],
        labels: ['Total Spendings', 'Savings', 'Foregone Savings'],
        hideHover: 'auto',
        resize: true,
        pointSize:5,
        lineWidth:4,
        lineColors: ['#52a7e0','#2ecc71','#f39c12'],
    });
    
    Morris.Bar({
        element: 'morris_bar_chart',
        data: [{ y: 'No of Suppliers', a: 60, b: 50, c:120 }],
        xkey: 'y',
        ykeys: ['a', 'b', 'c'],
        labels: ['Series A', 'Series B', 'Series C'],
        hideHover: 'auto',
        resize: true,
        barColors: ['#2ecc71', '#3498db', '#e74c3c'],
    });
});