<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<!-- START SIDEBAR-->
        <nav class="page-sidebar" id="sidebar">
            <div id="sidebar-collapse">
                <div class="admin-block d-flex">
                    <div>
                     <img src='<c:url value="/resources/assets2/img/admin-avatar.png" />' alt="CDEVendor" width="45">
                    </div>
                    <div class="admin-info">
                        <div class="font-strong">James Brown</div><small>Administrator</small></div>
                </div>
                <ul class="side-menu metismenu">
                    <li>
                    	<a href="/CDEVendor/dashboard"><i class="sidebar-item-icon fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
                    </li>
                    <li>
						<a href="/CDEVendor/itemprice"><i class="sidebar-item-icon fa fa-table"></i><span class="nav-label">Request Price For Items</span></a>
                    </li>
                    <li>
						<a href="/CDEVendor/polist"><i class="sidebar-item-icon fa fa-table"></i><span class="nav-label">PO List</span></a>
                    </li>
                    <li>
						<a href="/CDEVendor/dcitems"><i class="sidebar-item-icon fa fa-table"></i><span class="nav-label">DC Items</span></a>
                    </li>
                   <!-- <li>
                        <a class="" href="#"><i class="sidebar-item-icon fa fa-table"></i>
                            <span class="nav-label">Reports ></span>
                        </a>
                        <ul class="side-menu metismenu">
                            <li><a class="" href="/CDEProcurement/openindentreport"><i class="sidebar-item-icon fa fa-table"></i><span class="nav-label">Open Indent Report</span></a></li>
                            <li><a class="" href="/CDEProcurement/postatusreport"><i class="sidebar-item-icon fa fa-table"></i><span class="nav-label">PO Status Report</span></a></li>
                            <li><a class="" href="#"><i class="sidebar-item-icon fa fa-table"></i><span class="nav-label">Vendor Performance Report</span></a></li>
                        
                        </ul>
                    </li> -->
                </ul>
            </div>
        </nav>
        <!-- END SIDEBAR-->