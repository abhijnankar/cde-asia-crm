<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<jsp:include page="/vendorheader.jsp"></jsp:include>
<jsp:include page="/vendorsidebar.jsp"></jsp:include>
<%-- welcome <%=session.getAttribute("uname") %> --%>

<div class="content-wrapper">
	<!-- START PAGE CONTENT-->
	<div class="page-content fade-in-up">
		<div class="row">

			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-head" style="background-color: #0077c0;">
						<div class="ibox-title" style="color: white;">Vendor Pricing List</div>
						<!-- <div class="ibox-title"><input type="text" id="indentSearchDiv" style="height: 25px; font-size: 11px;" placeholder="Search ....." size="50px"/></div> -->
					</div>
                   
					<div class="ibox-body">
					<form:form id="editTableform1" method="post" action="/CDEVendor/lotupdate">
						<table class="table table-striped table-bordered table-hover"
							id="pricingDataTable" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Item Code</th>
									<th>Item Description</th>
									<th>Item Type</th>
									<!-- <th>Unit Price</th> -->
									<th>Your Price</th>
									<th>Lead Time (In Days)</th>
									<th>UOM</th>
									<th>Weight</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>
									<form:form id="editTableform" method="post" action="/CDEVendor/editPrice">
									</form:form>
								</td>
							</tr>
								<c:forEach items="${pricinglist}" var="pricinglist">
									<tr>
										<td>${pricinglist.itemCode}<input type="hidden" name="itemCode[]" value="${pricinglist.itemCode}"></td>
										<td>${pricinglist.itemDescription}</td>
										<td>
										<c:if test="${pricinglist.itemType == 'Y'}">										
												<% out.println("Fabrication(CDE-Asia)"); %>
										</c:if>
										<c:if test="${pricinglist.itemType == 'X'}">											
												<% out.println("Purchase(CDE-Asia)"); %>
										</c:if>
										<c:if test="${pricinglist.itemType == 'C'}">											
												<% out.println("Fabrication(Global)"); %>
										</c:if>
										<c:if test="${pricinglist.itemType == 'P'}">											
												<% out.println("Purchase(Global)"); %>
										</c:if>
										
										<%-- ${pricinglist.itemType} --%>
										
										
										</td>
										<%-- <td>${pricinglist.unitPrice}</td> --%>
										<td><input type="number" min="0" name="vendorprice[]" value="${pricinglist.vendorprice}"></td>
										<td><input type="number" min="0" name="leadtime[]" value="${pricinglist.leadtime}"></td>
										<td>${pricinglist.uom}</td>
										
										<td>${pricinglist.weight}</td>
                                        <td>
                                        <c:if test="${(not empty pricinglist.uploadfile) && (pricinglist.uploadfile != 'null') }">
                                        	<a download target="_blank" href="downloads/${pricinglist.uploadfile}">${pricinglist.uploadfile}</a>
                                        </c:if>
                                         <form:form id="editTableform" method="post" action="/CDEVendor/editPrice">
                                       		<button	onclick="editList('${pricinglist.itemCode}')" class="btn btn-info btn-sm">EditPrice</button>
												<input type="hidden"  name="itemCode"
													value="${pricinglist.itemCode}" />
													<input type="hidden"  name="vendorprice"
													value="${pricinglist.vendorprice}" />
                                          </form:form>
                                        
                                        
                                        </td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						
						<button type="submit" class="btn btn-info btn-sm" style="margin: 20px;">Update Price</button>
						</form:form>
					</div>
                 
				</div>
			</div>

		</div>
	</div>
	<!-- END PAGE CONTENT-->

<jsp:include page="/vendorfooter.jsp"></jsp:include>
<script>
	$(document).ready(function() {
		var purchaseTable = $('#pricingDataTable').DataTable({
			
			pageLength : 5,
		});
	})
	
	$(document).ready(function() {
		var purchaseTable = $('#poListDataTable').DataTable({
			
			pageLength : 5,
		});
	})
	
	function editList(itemCode){	
		alert(itemCode);
		document.getElementById("editTableform").submit();
	}
	
	
	
	function poList(indentID,projectID,poNumber){	
		alert(poNumber);
		document.getElementById("poTableform").submit();
	}
	</script>