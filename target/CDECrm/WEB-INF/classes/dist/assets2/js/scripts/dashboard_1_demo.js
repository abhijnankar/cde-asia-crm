$(function() {
	  var doughnutData = {
		  labels: ["Appliances","Automative","Computers","Electronics" ],
		  datasets: [{
			  data: [27,30,23,20],
			  backgroundColor: ["rgb(255, 99, 132)","rgb(54, 162, 235)","rgb(255, 205, 86)","rgb(46,204,113)"]
		  }]
	  } ;
	  var doughnutOptions = {
		  responsive: true,
		  legend: {
			display: true
		  },
	  };
	  var ctx4 = document.getElementById("doughnut_chart").getContext("2d");
	  new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});
  
	  // Bar Chart example

	var barData = {
		labels: ["NA", "EUR", "ASIA", "SA", "AUS", "NZ", "USA"],
		datasets: [
			{
				label: "Cash To Cash Cycle Time",
				backgroundColor:'#007bff',
				data: [45, 80, 58, 74, 54, 59, 40]
			},
			{
				label: "Account Rec. Days",
				backgroundColor: '#2ecc71',
				borderColor: "#fff",
				data: [29, 48, 40, 19, 78, 31, 85]
			},
			{
				label: "Inventory Days",
				backgroundColor: '#f39c12',
				borderColor: "#fff",
				data: [29, 48, 40, 19, 78, 31, 85]
			},
			{
				label: "Account Payable Days",
				backgroundColor: '#e74c3c',
				borderColor: "#fff",
				data: [29, 48, 40, 19, 78, 31, 85]
			}
		]
	};
	var barOptions = {
		responsive: true,
		maintainAspectRatio: false
	};

	var ctx = document.getElementById("bar_chart").getContext("2d");
	new Chart(ctx, {type: 'bar', data: barData, options:barOptions}); 
	
	// spark line
	$('.sparkline').sparkline();
	
	// data table
	$('#example-table,#inventory-table,#kpi-table').DataTable({
		pageLength: 10
	});
});