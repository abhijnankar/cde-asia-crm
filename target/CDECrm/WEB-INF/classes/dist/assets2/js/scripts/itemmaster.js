$(function() {

	// data table
	$('#itemmaster-table').DataTable({
		pageLength: 10,
		order: [[ 1, "desc" ]],
		"oLanguage": {
		  "sSearch": "Search Item Codes:"
		},
		"columnDefs": [{
			"targets": [0],
			"orderable": false
		}]
	});
	
	// 
	var rows = $('#itemmaster-table tr');
	
	$("input[data-select='all']").change(function(){
        $(this).prop('checked') 
            ? rows.find('.mail-check').prop('checked',true)
            : rows.find('.mail-check').prop('checked',false)        
    });
});