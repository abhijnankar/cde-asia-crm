/**********************************************
    Dynamic field handler for the forms
**********************************************/



// Additional information add for Item-and-price-list page
$(document).ready(function () {
  var maxField = 4; //Input fields increment limitation
  var addButton = $('.addAditionalInfo'); //Add button selector
  var wrapper = $('.additionalWrapper'); //Input field wrapper
  var fieldHTML =
      '<div class="row no-gutters"><div class="col-6"><div class="form-group"> <input type="text" class="form-control"></div></div><div class="col-auto"> <a href="javascript:void(0);" class="btn btn-danger removeAditionalInfo">-</a></div></div>'; //New input field html 
  var x = 1; //Initial field counter is 1

  //Once add button is clicked
  $(addButton).click(function () {
      //Check maximum number of input fields
      if (x < maxField) {
          x++; //Increment field counter
          $(wrapper).append(fieldHTML); //Add field html
      }
  });

  //Once remove button is clicked
  $(wrapper).on('click', '.removeAditionalInfo', function (e) {
      e.preventDefault();
      $(this).parent().parent().remove(); //Remove field html
      x--; //Decrement field counter
  });
});