<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<footer class="page-footer" style="text-align: center">
                <div class="font-13">2019-2020 � <b>CDE Asia LTD</b> - All rights reserved.</div>
                <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
            </footer>
        </div>
    </div>


    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- END PAGA BACKDROPS-->
<!-- CORE PLUGINS-->
    <script src="<c:url value="/resources/assets2/vendors/jquery/dist/jquery.min.js" />"></script>
    <script src="<c:url value="/resources/assets2/vendors/popper.js/dist/umd/popper.min.js" />"></script>
    <script src="<c:url value="/resources/assets2/vendors/bootstrap/dist/js/bootstrap.min.js" />"></script>
     <script src="<c:url value="/resources/assets2/vendors/metisMenu/dist/metisMenu.min.js" />"></script>
    <script src="<c:url value="/resources/assets2/vendors/jquery-slimscroll/jquery.slimscroll.min.js" />"></script>
    <!-- PAGE LEVEL PLUGINS-->
    <script src="<c:url value="/resources/assets2/vendors/chart.js/dist/Chart.min.js" />"></script>
   <script src="<c:url value="/resources/assets2/vendors/jquery-sparkline/dist/jquery.sparkline.min.js" />"></script>
   
    <script src="<c:url value="/resources/assets2/vendors/DataTables/datatables.min.js" />"></script>
   <script src="<c:url value="/resources/assets2/vendors/morris.js/morris.min.js" />"></script>
  <script src="<c:url value="/resources/assets2/vendors/raphael/raphael.min.js" />"></script>
        
    <!-- CORE SCRIPTS-->
    <script src="<c:url value="/resources/assets2/js/app.min.js" />"></script>
    <!-- PAGE LEVEL SCRIPTS-->
     <script src="<c:url value="/resources/assets2/js/scripts/procurement.js" />"></script>
     
     

<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
	<script
		src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script
		src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
	<script
		src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

	<script type="text/javascript">
        Highcharts.chart('container_1', {
  chart: {
    size:'40%',
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  credits: {
    enabled: false
  },
  exporting: { enabled: false },
  title: {
    text: 'Current Inventory Statistics'
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      dataLabels: {
        enabled: false
      },
      showInLegend: true
    }
  },
  series: [{
    name: '',
    colorByPoint: true,
    data: [{
      name: 'Allocated',
      color: 'green',
      y: 70.00,
      sliced: false,
      selected: true
    },
     {
      name: 'Not Allocated',
      color: 'red',
      y: 30.00
    }]
  }]
});
    </script>

	
</body>

</html>